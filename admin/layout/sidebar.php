<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo $user['foto'] ? URL_USER.'img/'.$user['foto'] : URL_USER.'img/profile-default.jpg' ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $user['nama_karyawan']; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li  class="active">
            <a href="<?php echo URL_USER ?>" target="blank"><i class="fa fa-external-link"></i> <span>Go to Website</span></a>
        </li>
        <li>
            <a href="<?php echo URL_ADMIN ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
        </li>
        <li class="treeview">
            <a href="#">
              <i class="fa fa-desktop"></i> <span>Program Cling</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li>
                  <a href="<?php echo URL_ADMIN ?>controller/kunjungan_pelanggan"><i class="fa fa-xing"></i> <span>Kunjungan Pelanggan</span></a>
                  <a href="<?php echo URL_ADMIN ?>controller/pembayaran"><i class="fa fa-shopping-bag"></i> <span>Pembayaran</span></a>
              </li>
            </ul>
        </li>
        <li>
            <a href="<?php echo URL_ADMIN ?>controller/pesan"><i class="fa fa-envelope-o"></i> <span>Pesan</span></a>
        </li>
        <li>
            <a href="<?php echo URL_ADMIN ?>controller/member"><i class="fa fa-users"></i> <span>Member</span></a>
        </li>
        <li class="treeview">
            <a href="#">
              <i class="fa fa-bath"></i> <span>Perawatan</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li>
                  <a href="<?php echo URL_ADMIN ?>controller/kategori_treatment"><i class="fa fa-shower"></i> <span>Kategori Perawatan</span></a>
                  <a href="<?php echo URL_ADMIN ?>controller/treatment"><i class="fa fa-bath"></i> <span>Perawatan</span></a>
              </li>
            </ul>
        </li>
        <li>
            <a href="<?php echo URL_ADMIN ?>controller/promo"><i class="fa fa-gift"></i> <span>Promo</span></a>
        </li>
        <li>
            <a href="<?php echo URL_ADMIN ?>controller/blog"><i class="fa fa-wordpress"></i> <span>Blog</span></a>
        </li>
        <!-- <li class="treeview">
            <a href="#">
              <i class="fa fa-gear"></i> <span>Sistem Konfigurasi</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li>
                  <a href="<?php echo URL_ADMIN ?>"><i class="fa fa-user-o"></i> <span>Profil Pengguna</span></a>
              </li>
            </ul>
        </li> -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>