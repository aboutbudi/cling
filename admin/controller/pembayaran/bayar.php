<?php
    define('PAGE_TITLE', 'Kunjungn Pelanggan');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');

    // Initialize the session
    session_start();
    
        // If session variable is not set it will redirect to login page
        if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 2){
        header("location:".URL_USER."member_area/");
        exit;
    }
    
    include_once('../../../config/controller.php');

    $username = $_SESSION['username'];
    $query = "SELECT users.username,karyawan.*,DATE_FORMAT(karyawan.created_at, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN karyawan ON users.id = karyawan.id_user WHERE username= '".$username."'";
    $user =  selectDetail($query);

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $id_pembayaran = isset($_POST['id_pembayaran']) ? $_POST['id_pembayaran'] : "";
        $total_pembayaran = isset($_POST['total_biaya']) ? $_POST['total_biaya'] : "";
        $total_poin = isset($_POST['total_poin']) ? $_POST['total_poin'] : "";

        $query1 = "select a.*,DATE_FORMAT(a.waktu_checkout, \"%e %M %Y %r\") AS tanggal_checkout, b.nomor_kunjungan,c.id_pelanggan,c.nomor_pelanggan,c.nama_pelanggan from pembayaran a
                    inner join kunjungan_pelanggan b on a.id_kunjungan = b.id_kunjungan_pelanggan
                    left join pelanggan c on b.id_pelanggan=c.id_pelanggan where a.id_pembayaran=".$id_pembayaran;
        $pembayaran =  selectDetail($query1);

        $t=time();
        $waktu_pembayaran=date("Y-m-d H:i:s",$t);
        $status_pembayaran=3;

        $columns1 = array(
            'total_pembayaran'=>$total_pembayaran,
            'status_pembayaran'=>$status_pembayaran,
            'waktu_pembayaran'=>$waktu_pembayaran
        );
        
        $condition1 = "id_pembayaran = ".$id_pembayaran;
        update('pembayaran',$columns1, $condition1);

        $columns1 = array(
            'status_transaksi'=>$status_pembayaran
        );
        
        $condition2 = "id_kunjungan = ".$pembayaran['id_kunjungan'];
        update('transaksi_kunjungan',$columns1, $condition);

        if($pembayaran['id_pelanggan']==1){
        }else{
            $query2="SELECT *,count(id_poin_pelanggan) as id_poin_tersedia FROM poin_pelanggan where id_pelanggan =".$pembayaran['id_pelanggan'];
            $poin_tersedia= selectDetail($query2);
            if($poin_tersedia['id_poin_tersedia']>0){
                $poin_saat_ini = $poin_tersedia['jumlah_poin'];
                $poin_sekarang = $poin_saat_ini +$total_poin;
             
                $jumlah_poin = $poin_sekarang;
                $columns3 = array(
                    'jumlah_poin'=>$jumlah_poin
                );
                $condition3 = "id_poin_pelanggan = ".$poin_tersedia['id_poin_pelanggan'];
                update('poin_pelanggan',$columns3, $condition3);
            }else{
                $values3 = array($pembayaran['id_pelanggan'],$total_poin);
                $columns3 = array('id_pelanggan','jumlah_poin');
                
                insert('poin_pelanggan', $values3, $columns3);
            }
        }

        echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."controller/pembayaran/detail.php?id_pembayaran=".$id_pembayaran."'>";
    }

    $content_page='../../pages/detail_pembayaran.php';

    include_once('../../layout/main_layout.php');
?>