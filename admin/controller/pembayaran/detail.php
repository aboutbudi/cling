<?php
    define('PAGE_TITLE', 'Kunjungn Pelanggan');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');

    // Initialize the session
    session_start();
    
        // If session variable is not set it will redirect to login page
        if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 2){
        header("location:".URL_USER."member_area/");
        exit;
    }
    
    include_once('../../../config/controller.php');

    $username = $_SESSION['username'];
    $query = "SELECT users.username,karyawan.*,DATE_FORMAT(karyawan.created_at, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN karyawan ON users.id = karyawan.id_user WHERE username= '".$username."'";
    $user =  selectDetail($query);

    if($_SERVER['REQUEST_METHOD'] == 'GET'){
        $id_pembayaran = isset($_GET['id_pembayaran']) ? $_GET['id_pembayaran'] : "";

        $query1 = "select a.*,DATE_FORMAT(a.waktu_checkout, \"%e %M %Y %r\") AS tanggal_checkout, b.nomor_kunjungan,c.nomor_pelanggan,c.nama_pelanggan from pembayaran a
                    inner join kunjungan_pelanggan b on a.id_kunjungan = b.id_kunjungan_pelanggan
                    left join pelanggan c on b.id_pelanggan=c.id_pelanggan where a.id_pembayaran=".$id_pembayaran;
        $pembayaran =  selectDetail($query1);

        $query2 = "SELECT b.*,DATE_FORMAT(b.waktu_transaksi, \"%e %M %Y %r\") AS tanggal_transaksi,c.nama_perawatan
                    from kunjungan_pelanggan a 
                    inner join transaksi_kunjungan b on a.id_kunjungan_pelanggan=b.id_kunjungan
                    inner join perawatan c on b.id_perawatan=c.id_perawatan where a.id_kunjungan_pelanggan=".$pembayaran['id_kunjungan'];
        $list_transaksi=selectBySql($query2);

        $query3="SELECT  SUM(b.poin) as total_poin,SUM(b.biaya) as total_biaya 
                    from kunjungan_pelanggan a 
                    inner join transaksi_kunjungan b on a.id_kunjungan_pelanggan=b.id_kunjungan
                    where a.id_kunjungan_pelanggan=".$pembayaran['id_kunjungan'];
        $jumlah=selectDetail($query3);
    }

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        // $kode_voucher = isset($_POST['kode_voucher']) ? $_POST['kode_voucher'] : "";

        //     $query4="select * from voucher a 
        //             inner join promo b on a.id_promo = b.id_promo
        //             where b.tanggal_mulai <= CURDATE() AND b.tanggal_selesai >= CURDATE() 
        //             and a.status_valid='Y'and a.kode_voucher='".$kode_voucher."'";
        //     $voucher=selectDetail($query4);
    }

    $content_page='../../pages/detail_pembayaran.php';

    include_once('../../layout/main_layout.php');
?>