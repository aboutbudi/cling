<?php
    define('PAGE_TITLE', 'Pembayaran');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');

    // Initialize the session
    session_start();
    
        // If session variable is not set it will redirect to login page
        if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 2){
        header("location:".URL_USER."member_area/");
        exit;
    }
    
    include_once('../../../config/controller.php');

    $username = $_SESSION['username'];
    $query = "SELECT users.username,karyawan.*,DATE_FORMAT(karyawan.created_at, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN karyawan ON users.id = karyawan.id_user WHERE username= '".$username."'";
    $user =  selectDetail($query);

    $pembayaran = array();
    $query = "select a.*,DATE_FORMAT(a.waktu_checkout, \"%e %M %Y %r\") AS tanggal_checkout, b.nomor_kunjungan,c.nomor_pelanggan,c.nama_pelanggan from pembayaran a
                inner join kunjungan_pelanggan b on a.id_kunjungan = b.id_kunjungan_pelanggan
                left join pelanggan c on b.id_pelanggan=c.id_pelanggan where a.status_pembayaran<>3";
    $pembayaran =  selectBySql($query);

    $pembayaran_lunas = array();
    $query = "select a.*,DATE_FORMAT(a.waktu_pembayaran, \"%e %M %Y %r\") AS tanggal_pembayaran, b.nomor_kunjungan,c.nomor_pelanggan,c.nama_pelanggan from pembayaran a
                inner join kunjungan_pelanggan b on a.id_kunjungan = b.id_kunjungan_pelanggan
                left join pelanggan c on b.id_pelanggan=c.id_pelanggan where a.status_pembayaran=3";
    $pembayaran_lunas =  selectBySql($query);
    

    $content_page='../../pages/list_pembayaran.php';

    include_once('../../layout/main_layout.php');
?>