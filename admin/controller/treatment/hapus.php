<?php
    define('PAGE_TITLE', 'Perawatan > Hapus');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');
    define('PATH_FILE', $_SERVER["DOCUMENT_ROOT"].'/cling/upload/');

    // Initialize the session
    session_start();
    
        // If session variable is not set it will redirect to login page
        if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 2){
        header("location:".URL_USER."member_area/");
        exit;
    }
    
    include_once('../../../config/controller.php');

    $username = $_SESSION['username'];
    $query = "SELECT users.username,karyawan.*,DATE_FORMAT(karyawan.created_at, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN karyawan ON users.id = karyawan.id_user WHERE username= '".$username."'";
    $user =  selectDetail($query);
    
    $content_page='../../pages/hapus_perawatan.php';

    
    if($_SERVER['REQUEST_METHOD'] == 'GET'){
        $id_perawatan = isset($_GET['id_perawatan']) ? $_GET['id_perawatan'] : "";
        
        $query = "SELECT perawatan.*, kategori_perawatan.nama AS kategori_perawatan FROM perawatan INNER JOIN kategori_perawatan ON perawatan.id_kategori_perawatan= kategori_perawatan.id_kategori_perawatan WHERE perawatan.id_perawatan=" .$id_perawatan;
        $perawatan = selectDetail($query);
    }

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $id_perawatan = isset($_POST['id_perawatan']) ? $_POST['id_perawatan'] : "";

        $condition = "id_perawatan = ".$id_perawatan;
        delete('perawatan', $condition);
        echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."controller/treatment/'>";
    }
    
    

    include_once('../../layout/main_layout.php');
?>