<?php
    define('PAGE_TITLE', 'Perawatan > Tambah');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');

    // Initialize the session
    session_start();
    
        // If session variable is not set it will redirect to login page
        if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 2){
        header("location:".URL_USER."member_area/");
        exit;
    }
    
    include_once('../../../config/controller.php');

    $username = $_SESSION['username'];
    $query = "SELECT users.username,karyawan.*,DATE_FORMAT(karyawan.created_at, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN karyawan ON users.id = karyawan.id_user WHERE username= '".$username."'";
    $user =  selectDetail($query);
    
    $content_page='../../pages/form_perawatan.php';

    $kategori_perawatan = array();
    $kategori_perawatan = select('kategori_perawatan');
    
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $id_kategori_perawatan = isset($_POST['id_kategori_perawatan']) ? $_POST['id_kategori_perawatan'] : "";
        $nama_perawatan = isset($_POST['nama_perawatan']) ? $_POST['nama_perawatan'] : "";
        $fasilitas = isset($_POST['fasilitas']) ? $_POST['fasilitas'] : "";
        $deskripsi = isset($_POST['deskripsi']) ? $_POST['deskripsi'] : "";
        $harga = isset($_POST['harga']) ? $_POST['harga'] : "";
        $poin = isset($_POST['poin']) ? $_POST['poin'] : "";
        
        $nama_file = $_FILES['foto']['name'];
        $ukuran_file = $_FILES['foto']['size'];
        $tipe_file = $_FILES['foto']['type'];
        $tmp_file = $_FILES['foto']['tmp_name'];

        $uploaddir = '../../../upload/';
        if($nama_file==""){
            $values = array($id_kategori_perawatan,$nama_perawatan,$fasilitas,$deskripsi,$harga,$poin);
            $columns = array('id_kategori_perawatan','nama_perawatan','fasilitas','deskripsi','harga','poin');
            
            insert('perawatan', $values, $columns);
            echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."controller/treatment/'>";
        }else{
            $foto = 'treatment-'.$nama_file;
            $uploadfile = $uploaddir . $foto;
            if($tipe_file == "image/jpg" ||$tipe_file == "image/jpeg" || $tipe_file == "image/png"){
                if($ukuran_file <= 1000000){
                    if(move_uploaded_file($tmp_file, $uploadfile)){
                        $values = array($id_kategori_perawatan,$nama_perawatan,$fasilitas,$deskripsi,$harga,$poin,$foto);
                        $columns = array('id_kategori_perawatan','nama_perawatan','fasilitas','deskripsi','harga','poin','foto');
                        
                        insert('perawatan', $values, $columns);
                        echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."controller/treatment/'>";
                    }else{
                        echo "<script>alert(\"File gagal diupload\");</script>";
                    }
                }else{
                    echo "<script>alert(\"Ukuran file terlalu besar\");</script>";
                }
            }else{
                echo "<script>alert(\"File yang anda masukan bukan gambar\");</script>";
            }
        }
        
        
        
} 
    
    

    include_once('../../layout/main_layout.php');
?>