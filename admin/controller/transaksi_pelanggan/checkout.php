<?php
    define('PAGE_TITLE', 'Check Out');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');

    // Initialize the session
    session_start();
    
        // If session variable is not set it will redirect to login page
        if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 2){
        header("location:".URL_USER."member_area/");
        exit;
    }
    
    include_once('../../../config/controller.php');

    $username = $_SESSION['username'];
    $query = "SELECT users.username,karyawan.*,DATE_FORMAT(karyawan.created_at, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN karyawan ON users.id = karyawan.id_user WHERE username= '".$username."'";
    $user =  selectDetail($query);

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $id_kunjungan = isset($_POST['id_kunjungan']) ? $_POST['id_kunjungan'] : "";
        $total_harga = isset($_POST['total_harga']) ? $_POST['total_harga'] : "";
        $total_poin = isset($_POST['total_poin']) ? $_POST['total_poin'] : "";
        
        $status_transaksi=2;
        $columns1 = array(
            'status_transaksi'=>$status_transaksi
        );
        
        $condition = "id_kunjungan = ".$id_kunjungan;
        update('transaksi_kunjungan',$columns1, $condition);

        $values = array($id_kunjungan,$total_harga);
        $columns = array('id_kunjungan','total_harga');
        
        insert('pembayaran', $values, $columns);
        echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."controller/transaksi_pelanggan/index.php?id_kunjungan_pelanggan=".$id_kunjungan."'>";
    }

    $content_page='../../pages/list_transaksi_pelanggan.php';

    include_once('../../layout/main_layout.php');
?>
