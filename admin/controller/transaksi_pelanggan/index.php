<?php
    define('PAGE_TITLE', 'Kunjungn Pelanggan');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');

    // Initialize the session
    session_start();
    
        // If session variable is not set it will redirect to login page
        if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 2){
        header("location:".URL_USER."member_area/");
        exit;
    }
    
    include_once('../../../config/controller.php');

    $username = $_SESSION['username'];
    $query = "SELECT users.username,karyawan.*,DATE_FORMAT(karyawan.created_at, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN karyawan ON users.id = karyawan.id_user WHERE username= '".$username."'";
    $user =  selectDetail($query);

    $kunjungan_pelanggan = array();
    if($_SERVER['REQUEST_METHOD'] == 'GET'){
        //get anggota by id 
        $id_kunjungan_pelanggan = isset($_GET['id_kunjungan_pelanggan']) ? $_GET['id_kunjungan_pelanggan'] : "";

        $query2 = "SELECT a.*,DATE_FORMAT(a.waktu_kunjungan, \"%e %M %Y %r\") AS tanggal_kunjungan,b.nomor_pelanggan,b.nama_pelanggan FROM kunjungan_pelanggan a LEFT JOIN pelanggan b on a.id_pelanggan = b.id_pelanggan WHERE a.id_kunjungan_pelanggan=".$id_kunjungan_pelanggan;
        $kunjungan_pelanggan =  selectDetail($query2);

        $query3 = "SELECT b.*,DATE_FORMAT(b.waktu_transaksi, \"%e %M %Y %r\") AS tanggal_transaksi,c.nama_perawatan
                    from kunjungan_pelanggan a 
                    inner join transaksi_kunjungan b on a.id_kunjungan_pelanggan=b.id_kunjungan
                    inner join perawatan c on b.id_perawatan=c.id_perawatan where a.id_kunjungan_pelanggan=".$id_kunjungan_pelanggan;
        $list_transaksi=selectBySql($query3);
        $query4="SELECT  SUM(b.poin) as total_poin,SUM(b.biaya) as total_biaya 
                    from kunjungan_pelanggan a 
                    inner join transaksi_kunjungan b on a.id_kunjungan_pelanggan=b.id_kunjungan
                    where a.id_kunjungan_pelanggan=".$id_kunjungan_pelanggan;
        $jumlah=selectDetail($query4);

        $query5="select count(b.id_pembayaran) as id_pembayaran from kunjungan_pelanggan a inner join pembayaran b on a.id_kunjungan_pelanggan = b.id_kunjungan where b.id_kunjungan=".$id_kunjungan_pelanggan;
        $sudah_checkout=selectDetail($query5);
        
        $perawatan = array();
        $perawatan = select('perawatan');

    }

    $content_page='../../pages/list_transaksi_pelanggan.php';

    include_once('../../layout/main_layout.php');
?>