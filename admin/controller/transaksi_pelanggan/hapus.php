<?php
    define('PAGE_TITLE', 'Transaksi Pelanggan');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');

    // Initialize the session
    session_start();
    
        // If session variable is not set it will redirect to login page
        if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 2){
        header("location:".URL_USER."member_area/");
        exit;
    }
    
    include_once('../../../config/controller.php');

    $username = $_SESSION['username'];
    $query = "SELECT users.username,karyawan.*,DATE_FORMAT(karyawan.created_at, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN karyawan ON users.id = karyawan.id_user WHERE username= '".$username."'";
    $user =  selectDetail($query);

    if($_SERVER['REQUEST_METHOD'] == 'GET'){
        $id_kunjungan = isset($_GET['id_kunjungan']) ? $_GET['id_kunjungan'] : "";
        $id_transaksi_kunjungan = isset($_GET['id_transaksi_kunjungan']) ? $_GET['id_transaksi_kunjungan'] : "";
        
        $condition = "id_transaksi_kunjungan = ".$id_transaksi_kunjungan;
        delete('transaksi_kunjungan', $condition);
        echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."controller/transaksi_pelanggan/index.php?id_kunjungan_pelanggan=".$id_kunjungan."'>";
    }

    $content_page='../../pages/list_transaksi_pelanggan.php';

    include_once('../../layout/main_layout.php');
?>
