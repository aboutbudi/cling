<?php
    define('PAGE_TITLE', 'Kunjungan Pelanggan');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');

    // Initialize the session
    session_start();
    
        // If session variable is not set it will redirect to login page
        if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 2){
        header("location:".URL_USER."member_area/");
        exit;
    }
    
    include_once('../../../config/controller.php');

    $username = $_SESSION['username'];
    $query = "SELECT users.username,karyawan.*,DATE_FORMAT(karyawan.created_at, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN karyawan ON users.id = karyawan.id_user WHERE username= '".$username."'";
    $user =  selectDetail($query);

    

    $pelanggan = array();
    $pelanggan = select('pelanggan');

     if($_SERVER['REQUEST_METHOD'] == 'POST'){
        
        $id_pelanggan = isset($_POST['id_pelanggan']) ? $_POST['id_pelanggan'] : "";
        
        $query2 = "SELECT * FROM kunjungan_pelanggan order by id_kunjungan_pelanggan desc limit 1";
        $tes = selectDetail($query2);
        $nomor_kunjungan="";
        
        $last_record=$tes["id_kunjungan_pelanggan"];
        $no_kunjungan="";
        if($last_record<10){
            $no_kunjungan= date("dmy")."000".$last_record+1;
        }elseif($last_record>=10&&$last_record<100){
            $no_kunjungan= date("dmy")."00".$last_record+1;
        }else{
            $no_kunjungan= date("dmy")."0".$last_record+1;
        }
        if($id_pelanggan==1){
            $nomor_kunjungan="REG".$no_kunjungan;
        }else{
            $nomor_kunjungan="MBR".$no_kunjungan;
        }


        $values = array($id_pelanggan,$nomor_kunjungan);
        $columns = array('id_pelanggan','nomor_kunjungan');
        
        insert('kunjungan_pelanggan', $values, $columns);
        echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."controller/kunjungan_pelanggan/'>";
    } 

    $content_page='../../pages/form_kunjungan_pelanggan.php';

    include_once('../../layout/main_layout.php');
?>