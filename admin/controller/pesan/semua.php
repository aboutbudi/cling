<?php
    define('PAGE_TITLE', 'Pesan');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');

    include_once('../../../config/controller.php');

    // Initialize the session
    session_start();
    
    // If session variable is not set it will redirect to login page
    if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 2){
        header("location:".URL_USER."member_area/");
        exit;
    }
    $username = $_SESSION['username'];
    $query = "SELECT users.username,karyawan.*,DATE_FORMAT(karyawan.created_at, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN karyawan ON users.id = karyawan.id_user WHERE username= '".$username."'";
    $user =  selectDetail($query);

    $query_pesan_masuk = "SELECT pesan.*,DATE_FORMAT(waktu_kirim, \"%e %M %Y %r\") AS waktu_pengiriman,DATE_FORMAT(waktu_tanggapan, \"%e %M %Y %r\") AS waktu_balas,karyawan.nama_karyawan,pelanggan.nama_pelanggan,pelanggan.foto 
	FROM pesan
	INNER JOIN pelanggan on pesan.id_pelanggan=pelanggan.id_pelanggan
    INNER JOIN karyawan on pesan.id_penanggap=karyawan.id_karyawan
    WHERE id_penanggap IS NOT NULL ORDER BY waktu_kirim DESC";
    $pesan_masuk =  selectBySql($query_pesan_masuk);
    
    $query_jml_psn = "SELECT COUNT(id_pesan) AS jumlah_pesan_masuk FROM pesan WHERE id_penanggap IS NULL";
    $jml_pesan =  selectDetail($query_jml_psn);
    
    $query_jml_sent = "SELECT COUNT(id_pesan) AS jumlah_pesan_terkirim FROM pesan WHERE id_penanggap=".$user['id_karyawan'];
    $jml_sent =  selectDetail($query_jml_sent);
    
    $query_jml_sent_all = "SELECT COUNT(id_pesan) AS jumlah_pesan_terkirim FROM pesan WHERE id_penanggap IS NOT NULL";
    $jml_sent_all =  selectDetail($query_jml_sent_all);
    
    $content_page='../../pages/list_semua_pesan_terkirim.php';

    include_once('../../layout/main_layout.php');
?>