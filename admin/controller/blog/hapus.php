<?php
    define('PAGE_TITLE', 'Blog > Hapus');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');
    define('PATH_FILE', $_SERVER["DOCUMENT_ROOT"].'/cling/upload/');

    // Initialize the session
    session_start();
    
        // If session variable is not set it will redirect to login page
        if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 2){
        header("location:".URL_USER."member_area/");
        exit;
    }
    
    include_once('../../../config/controller.php');

    $username = $_SESSION['username'];
    $query = "SELECT users.username,karyawan.*,DATE_FORMAT(karyawan.created_at, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN karyawan ON users.id = karyawan.id_user WHERE username= '".$username."'";
    $user =  selectDetail($query);

    $content_page='../../pages/hapus_blog.php';

    
    if($_SERVER['REQUEST_METHOD'] == 'GET'){
        $id_blog = isset($_GET['id_blog']) ? $_GET['id_blog'] : "";
        
        $query = "SELECT * FROM blog  WHERE id_blog=" .$id_blog;
        $blog = selectDetail($query);
    }

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $id_blog = isset($_POST['id_blog']) ? $_POST['id_blog'] : "";

        $condition = "id_blog = ".$id_blog;
        delete('blog', $condition);
        echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."controller/blog/'>";
    }
    
    

    include_once('../../layout/main_layout.php');
?>