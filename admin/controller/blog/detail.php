<?php
    define('PAGE_TITLE', 'Blog > Detail');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');

    // Initialize the session
    session_start();
    
        // If session variable is not set it will redirect to login page
        if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 2){
        header("location:".URL_USER."member_area/");
        exit;
    }
    
    include_once('../../../config/controller.php');

    $username = $_SESSION['username'];
    $query = "SELECT users.username,karyawan.*,DATE_FORMAT(karyawan.created_at, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN karyawan ON users.id = karyawan.id_user WHERE username= '".$username."'";
    $user =  selectDetail($query);

    $id_blog  = $_GET["id_blog"];
    $query = "SELECT *, DATE_FORMAT(tanggal_post, \"%e %M %Y\") as tanggal_posting FROM blog WHERE blog.id_blog=" .$id_blog;
    $blog = selectDetail($query); 
    
    $content_page='../../pages/detail_blog.php';

    include_once('../../layout/main_layout.php');
?>