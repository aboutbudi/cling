<?php
    define('PAGE_TITLE', 'Blog > Tambah');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');
    define('PATH_FILE', $_SERVER["DOCUMENT_ROOT"].'/cling/upload/');

    // Initialize the session
    session_start();
    
        // If session variable is not set it will redirect to login page
        if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 2){
        header("location:".URL_USER."member_area/");
        exit;
    }
    
    include_once('../../../config/controller.php');

    $username = $_SESSION['username'];
    $query = "SELECT users.username,karyawan.*,DATE_FORMAT(karyawan.created_at, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN karyawan ON users.id = karyawan.id_user WHERE username= '".$username."'";
    $user =  selectDetail($query);
    
    $content_page='../../pages/form_blog.php';

    
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        
        $judul = isset($_POST['judul']) ? $_POST['judul'] : "";
        $konten = isset($_POST['konten']) ? $_POST['konten'] : "";
        $penulis = $user['nama_karyawan'];
        
        $nama_file = $_FILES['foto']['name'];
        $ukuran_file = $_FILES['foto']['size'];
        $tipe_file = $_FILES['foto']['type'];
        $tmp_file = $_FILES['foto']['tmp_name'];
        
        $uploaddir = '../../../upload/';
        if($nama_file==""){
            $values = array($judul,$konten,$penulis);
            $columns = array('judul', 'konten', 'penulis');
            
            insert('blog', $values, $columns);
            echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."controller/blog/'>";
        }else{
            $foto = 'blog-'.$nama_file;
            $uploadfile = $uploaddir . $foto;

            
            if($tipe_file == "image/jpg" ||$tipe_file == "image/jpeg" || $tipe_file == "image/png"){
                if($ukuran_file <= 1000000){
                    if(move_uploaded_file($tmp_file, $uploadfile)){
                        $values = array($judul,$konten,$penulis,$foto);
                        $columns = array('judul', 'konten', 'penulis', 'foto');
                        
                        insert('blog', $values, $columns);
                        echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."controller/blog/'>";
                    }else{
                        echo "<script>alert(\"File gagal diupload\");</script>";
                    }
                }else{
                    echo "<script>alert(\"Ukuran file terlalu besar\");</script>";
                }
            }else{
                echo "<script>alert(\"File yang anda masukan bukan gambar\");</script>";
            }
        }
        
        
        
    } 
    
    

    include_once('../../layout/main_layout.php');
?>