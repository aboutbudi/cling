<?php
    define('PAGE_TITLE', 'Member > Hapus');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');
    define('PATH_FILE', $_SERVER["DOCUMENT_ROOT"].'/cling/upload/');

    // Initialize the session
    session_start();
    
        // If session variable is not set it will redirect to login page
        if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 2){
        header("location:".URL_USER."member_area/");
        exit;
    }
    
    include_once('../../../config/controller.php');

    $username = $_SESSION['username'];
    $query = "SELECT users.username,karyawan.*,DATE_FORMAT(karyawan.created_at, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN karyawan ON users.id = karyawan.id_user WHERE username= '".$username."'";
    $user =  selectDetail($query);
    
    $content_page='../../pages/hapus_member.php';

    $pelanggan = array();
    if($_SERVER['REQUEST_METHOD'] == 'GET'){
        $id_pelanggan = isset($_GET['id_pelanggan']) ? $_GET['id_pelanggan'] : "";
        
        $query = "select pelanggan.*, DATE_FORMAT(tanggal_pendaftaran, \"%e %M %Y\") as tanggal_mendaftar, poin_pelanggan.jumlah_poin FROM pelanggan LEFT JOIN poin_pelanggan ON pelanggan.id_pelanggan = poin_pelanggan.id_pelanggan WHERE pelanggan.id_pelanggan=" .$id_pelanggan;
        $pelanggan_detail = selectDetail($query);
    }

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $id_pelanggan = isset($_POST['id_pelanggan']) ? $_POST['id_pelanggan'] : "";

        $condition = "id_pelanggan = ".$id_pelanggan;
        delete('pelanggan', $condition);
        echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."controller/member/'>";
    }
    
    

    include_once('../../layout/main_layout.php');
?>