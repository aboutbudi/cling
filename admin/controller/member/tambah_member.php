<?php
    define('PAGE_TITLE', 'Register');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');
    include_once('../../../config/link.php');
    include_once('../../../config/controller.php');

    // Initialize the session
    session_start();
    
        // If session variable is not set it will redirect to login page
        if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 2){
        header("location:".URL_USER."member_area/");
        exit;
    }

    $query2 = "SELECT * FROM pelanggan order by id_pelanggan desc limit 1";
    $tes = selectDetail($query2);

    $last_record=$tes["jumlah_pelanggan"];
    $no_reg="";
    if($last_record<10){
        $no_reg= date("dmy")."000".$last_record+1;
    }elseif($last_record>=10&&$last_record<100){
        $no_reg= date("dmy")."00".$last_record+1;
    }else{
        $no_reg= date("dmy")."0".$last_record+1;
    }
    
    $id_role=2;
    

    // Define variables and initialize with empty values
    $username = $password = $confirm_password = "";
    $username_err = $password_err = $confirm_password_err = "";

    if($_SERVER["REQUEST_METHOD"] == "POST"){

        $nama_pelanggan = isset($_POST['nama_pelanggan']) ? $_POST['nama_pelanggan'] : "";
        $email = isset($_POST['email']) ? $_POST['email'] : "";
        $nomor_telepon = isset($_POST['nomor_telepon']) ? $_POST['nomor_telepon'] : "";
        $alamat = isset($_POST['alamat']) ? $_POST['alamat'] : "";
        
        $nomor_pelanggan = $no_reg;
        
        $nama_file = $_FILES['foto']['name'];
        $ukuran_file = $_FILES['foto']['size'];
        $tipe_file = $_FILES['foto']['type'];
        $tmp_file = $_FILES['foto']['tmp_name'];

        $uploaddir = '../../../upload/';
 
        // Validate username
        if(empty(trim($_POST["username"]))){
            $username_err = "Please enter a username.";
        } else{
            // Prepare a select statement
            $sql = "SELECT id FROM users WHERE username = ?";
            
            if($stmt = mysqli_prepare($link, $sql)){
                // Bind variables to the prepared statement as parameters
                mysqli_stmt_bind_param($stmt, "s", $param_username);
                
                // Set parameters
                $param_username = trim($_POST["username"]);
                
                // Attempt to execute the prepared statement
                if(mysqli_stmt_execute($stmt)){
                    /* store result */
                    mysqli_stmt_store_result($stmt);
                    
                    if(mysqli_stmt_num_rows($stmt) == 1){
                        $username_err = "This username is already taken.";
                    } else{
                        $username = trim($_POST["username"]);
                    }
                } else{
                    echo "Oops! Something went wrong. Please try again later.";
                }
            }
            
            // Close statement
            mysqli_stmt_close($stmt);
        }
        
        // Validate password
        if(empty(trim($_POST['password']))){
            $password_err = "Please enter a password.";     
        } elseif(strlen(trim($_POST['password'])) < 6){
            $password_err = "Password must have atleast 6 characters.";
        } else{
            $password = trim($_POST['password']);
        }
        
        // Validate confirm password
        if(empty(trim($_POST["confirm_password"]))){
            $confirm_password_err = 'Please confirm password.';     
        } else{
            $confirm_password = trim($_POST['confirm_password']);
            if($password != $confirm_password){
                $confirm_password_err = 'Password did not match.';
            }
        }
        
        // Check input errors before inserting in database
        if(empty($username_err) && empty($password_err) && empty($confirm_password_err)){
            
            // Prepare an insert statement
            $sql = "INSERT INTO users (id_role,username, password) VALUES (?, ?, ?)";
            
            if($stmt = mysqli_prepare($link, $sql)){
                // Bind variables to the prepared statement as parameters
                mysqli_stmt_bind_param($stmt, "sss",$param_id_role, $param_username, $param_password);
                
                // Set parameters
                $param_id_role = $id_role;
                $param_username = $username;
                $param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash
                
                // Attempt to execute the prepared statement
                if(mysqli_stmt_execute($stmt)){
                    $query3 = "SELECT * FROM users where username='".$username."'";
                    $hasil_user = selectDetail($query3);
                    if($nama_file==""){
                        $values = array($hasil_user['id'],$nomor_pelanggan,$nama_pelanggan,$email,$nomor_telepon,$alamat,$foto);
                        $columns = array('id_user','nomor_pelanggan', 'nama_pelanggan', 'email', 'nomor_telepon', 'alamat', 'foto');
                        
                        insert('pelanggan', $values, $columns);

                        echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."controller/member/'>";
                    }else{
                        $foto = $nomor_pelanggan.'-'.$nama_file;
                        $uploadfile = $uploaddir . $foto;

                        
                        if($tipe_file == "image/jpg" ||$tipe_file == "image/jpeg" || $tipe_file == "image/png"){
                            if($ukuran_file <= 1000000){
                                if(move_uploaded_file($tmp_file, $uploadfile)){
                                    $values = array($hasil_user['id'],$nomor_pelanggan,$nama_pelanggan,$email,$nomor_telepon,$alamat,$foto);
                                    $columns = array('id_user','nomor_pelanggan', 'nama_pelanggan', 'email', 'nomor_telepon', 'alamat', 'foto');
                                    
                                    insert('pelanggan', $values, $columns);
                                    // Redirect to login page
                                    header("location: ".URL_ADMIN."controller/member");
                                }else{
                                    echo "<script>alert(\"File gagal diupload\");</script>";
                                }
                            }else{
                                echo "<script>alert(\"Ukuran file terlalu besar\");</script>";
                            }
                        }else{
                            echo "<script>alert(\"File yang anda masukan bukan gambar\");</script>";
                        }
                    }
                } else{
                    echo "Something went wrong. Please try again later.";
                }
            }
            
            // Close statement
            mysqli_stmt_close($stmt);
        }
        
        // Close connection
        mysqli_close($link);
    }

    $content_page='../../pages/form_member.php';
    include_once('../../layout/main_layout.php');
?>