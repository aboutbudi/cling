<?php
    define('PAGE_TITLE', 'Member > Detail');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');

    // Initialize the session
    session_start();
    
        // If session variable is not set it will redirect to login page
        if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 2){
        header("location:".URL_USER."member_area/");
        exit;
    }
    
    include_once('../../../config/controller.php');

    $username = $_SESSION['username'];
    $query = "SELECT users.username,karyawan.*,DATE_FORMAT(karyawan.created_at, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN karyawan ON users.id = karyawan.id_user WHERE username= '".$username."'";
    $user =  selectDetail($query);

    $id_pelanggan  = $_GET["id_pelanggan"];
    $query = "select *, DATE_FORMAT(tanggal_pendaftaran, \"%e %M %Y\") as tanggal_mendaftar, poin_pelanggan.jumlah_poin FROM pelanggan LEFT JOIN poin_pelanggan ON pelanggan.id_pelanggan = poin_pelanggan.id_pelanggan WHERE pelanggan.id_pelanggan=" .$id_pelanggan;
    $pelanggan_detail = selectDetail($query); 
    
    $content_page='../../pages/detail_member.php';

    include_once('../../layout/main_layout.php');
?>