<?php
    define('PAGE_TITLE', 'Kategori Perawatan > Detail');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');

    // Initialize the session
    session_start();
    
        // If session variable is not set it will redirect to login page
        if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 2){
        header("location:".URL_USER."member_area/");
        exit;
    }
    
    include_once('../../../config/controller.php');

    $username = $_SESSION['username'];
    $query = "SELECT users.username,karyawan.*,DATE_FORMAT(karyawan.created_at, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN karyawan ON users.id = karyawan.id_user WHERE username= '".$username."'";
    $user =  selectDetail($query);

    $id_kategori_perawatan  = $_GET["id_kategori_perawatan"];
    $query = "select * FROM kategori_perawatan WHERE id_kategori_perawatan=" .$id_kategori_perawatan;
    $kategori_perawatan = selectDetail($query); 
    
    $content_page='../../pages/detail_kategori_perawatan.php';

    include_once('../../layout/main_layout.php');
?>