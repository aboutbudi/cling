<?php
    define('PAGE_TITLE', 'Kategori Perawatan > Ubah');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');
    define('PATH_FILE', $_SERVER["DOCUMENT_ROOT"].'/cling/upload/');

    // Initialize the session
    session_start();
    
        // If session variable is not set it will redirect to login page
        if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 2){
        header("location:".URL_USER."member_area/");
        exit;
    }
    
    include_once('../../../config/controller.php');

    $username = $_SESSION['username'];
    $query = "SELECT users.username,karyawan.*,DATE_FORMAT(karyawan.created_at, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN karyawan ON users.id = karyawan.id_user WHERE username= '".$username."'";
    $user =  selectDetail($query);
    
    $content_page='../../pages/form_kategori_perawatan.php';

    $kategori_perawatan = array();
    if($_SERVER['REQUEST_METHOD'] == 'GET'){
        //get anggota by id 
        $id_kategori_perawatan = isset($_GET['id_kategori_perawatan']) ? $_GET['id_kategori_perawatan'] : "";
        $condition = 'id_kategori_perawatan='.$id_kategori_perawatan;
        $results = select('kategori_perawatan', null, $condition);
        $kategori_perawatan = count($results) > 0 ? $results[0] : array();
    }

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $id_kategori_perawatan = isset($_POST['id_kategori_perawatan']) ? $_POST['id_kategori_perawatan'] : "";
        $nama = isset($_POST['nama']) ? $_POST['nama'] : "";
        $deskripsi = isset($_POST['deskripsi']) ? $_POST['deskripsi'] : "";
                
        $columns = array(
            'nama'=>$nama,
            'deskripsi'=>$deskripsi
        );
        
        $condition = "id_kategori_perawatan = ".$id_kategori_perawatan;
        update('kategori_perawatan',$columns, $condition);
        echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."controller/kategori_treatment/'>";
} 
    
    

    include_once('../../layout/main_layout.php');
?>