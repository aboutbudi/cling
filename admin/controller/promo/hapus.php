<?php
    define('PAGE_TITLE', 'Perawatan > Tambah');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');

    // Initialize the session
    session_start();
    
        // If session variable is not set it will redirect to login page
    if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 2){
        header("location:".URL_USER."member_area/");
        exit;
    }
    
    include_once('../../../config/controller.php');

    $username = $_SESSION['username'];
    $query = "SELECT users.username,karyawan.*,DATE_FORMAT(karyawan.created_at, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN karyawan ON users.id = karyawan.id_user WHERE username= '".$username."'";
    $user =  selectDetail($query);
    
    $content_page='../../pages/hapus_promo.php';

    $perawatan = array();
    $perawatan = select('perawatan');

    $promo = array();
    if($_SERVER['REQUEST_METHOD'] == 'GET'){
        //get anggota by id 
        $id_promo = isset($_GET['id_promo']) ? $_GET['id_promo'] : "";
        $condition = 'id_promo='.$id_promo;
        $results = select('promo', null, $condition);
        $promo = count($results) > 0 ? $results[0] : array();

        $query = "SELECT * FROM perawatan WHERE id_perawatan = ".$promo['id_perawatan'];
        $nama_perawatan = selectDetail($query);
    }

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $id_promo = isset($_POST['id_promo']) ? $_POST['id_promo'] : "";

        $condition = "id_promo = ".$id_promo;
        delete('promo', $condition);
        echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."controller/promo/'>";
    }
    include_once('../../layout/main_layout.php');
?>