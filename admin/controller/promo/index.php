<?php
    define('PAGE_TITLE', 'Treatment');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');

    // Initialize the session
    session_start();
    
        // If session variable is not set it will redirect to login page
        if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 2){
        header("location:".URL_USER."member_area/");
        exit;
    }
    
    include_once('../../../config/controller.php');

    $username = $_SESSION['username'];
    $query = "SELECT users.username,karyawan.*,DATE_FORMAT(karyawan.created_at, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN karyawan ON users.id = karyawan.id_user WHERE username= '".$username."'";
    $user =  selectDetail($query);
    
    $promo = array();
    $query = "SELECT promo.*, DATE_FORMAT(promo.tanggal_mulai, \"%e %M %Y\") as tanggal_mulai, DATE_FORMAT(promo.tanggal_selesai, \"%e %M %Y\") as tanggal_selesai,perawatan.nama_perawatan FROM promo LEFT JOIN perawatan ON promo.id_perawatan = perawatan.id_perawatan WHERE promo.tanggal_mulai <= CURDATE() AND promo.tanggal_selesai >= CURDATE();";
    $promo =  selectBySql($query);
    
    $content_page='../../pages/list_promo.php';

    include_once('../../layout/main_layout.php');
?>