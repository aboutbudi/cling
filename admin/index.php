<?php
    define('PAGE_TITLE', 'Dashboard');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');
    // Initialize the session
    session_start();
    
    // If session variable is not set it will redirect to login page
    if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 2){
            header("location:".URL_USER."member_area/");
            exit;
    }
    include_once('../config/controller.php');
    
    $username = $_SESSION['username'];
    $query = "SELECT users.username,karyawan.*,DATE_FORMAT(karyawan.created_at, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN karyawan ON users.id = karyawan.id_user WHERE username= '".$username."'";
    $user =  selectDetail($query);
    
    $blog = array();
    $query = "SELECT *, DATE_FORMAT(tanggal_post, \"%e %M %Y\") as tanggal_posting FROM blog LIMIT 5";
    $blog =  selectBySql($query);

    $pelanggan = array();
    $query2 = "SELECT *,DATE_FORMAT(tanggal_pendaftaran, \"%e %b %y\") as tanggal_gabung FROM pelanggan LIMIT 8";
    $pelanggan =  selectBySql($query2);

    $perawatan = array();
    $query3 = "SELECT perawatan.*, kategori_perawatan.nama AS kategori_perawatan FROM perawatan INNER JOIN kategori_perawatan ON perawatan.id_kategori_perawatan = kategori_perawatan.id_kategori_perawatan LIMIT 5";
    $perawatan =  selectBySql($query3);

    $query_pesan_masuk = "SELECT pesan.*,DATE_FORMAT(waktu_kirim, \"%e %M %Y %r\") AS waktu_pengiriman,pelanggan.nama_pelanggan,pelanggan.foto 
	FROM pesan
	INNER JOIN pelanggan on pesan.id_pelanggan=pelanggan.id_pelanggan
    WHERE id_penanggap IS NULL ORDER BY waktu_kirim DESC";
    $pesan_masuk =  selectBySql($query_pesan_masuk);
    $query_jml_psn = "SELECT COUNT(id_pesan) AS jumlah_pesan_masuk FROM pesan WHERE id_penanggap IS NULL";
    $jml_pesan =  selectDetail($query_jml_psn);

    $kunjungan_pelanggan = array();
    $query_kunjungan_pelanggan = "SELECT b.foto,a.*,DATE_FORMAT(a.waktu_kunjungan, \"%e %M %Y %r\") AS tanggal_kunjungan,b.nomor_pelanggan,b.nama_pelanggan 
                FROM kunjungan_pelanggan a 
                INNER JOIN pelanggan b on a.id_pelanggan = b.id_pelanggan order by waktu_kunjungan desc LIMIT 5";
    $kunjungan_pelanggan =  selectBySql($query_kunjungan_pelanggan);
    
    $content_page='pages/dashboard.php';

    include_once('layout/main_layout.php');
?>