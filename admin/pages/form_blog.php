<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Blog
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Blog</li><li class="active"><?php echo $blog['id_blog'] ? 'Ubah' : 'Tambah'; ?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header">
                        <a href="<?php echo URL_ADMIN."controller/blog/"?>" class="pull-right btn btn-sm btn-danger" tooltip="view" alt="view"><i class="fa fa-backward"></i> Kembali</a>
                        <h3 class="box-title"><?php echo $blog['id_blog'] ? 'Ubah Posting' : 'Tambah Posting'; ?></h3>
                    </div>
                    <div class="box-body">
                        <form enctype="multipart/form-data" action="<?php echo $blog['id_blog'] ? URL_ADMIN."controller/blog/ubah.php" : URL_ADMIN."controller/blog/tambah.php"; ?>" method="post" class="form-horizontal">
                            <div class="form-group">
                                <label for="judul" class="col-sm-2 control-label">Judul</label>
                                <div class="col-sm-10">
                                    <input type="hidden" class="form-control" id="id_blog" name="id_blog" value="<?php echo !empty($blog) ? $blog['id_blog'] : ''; ?>">
                                    <input type="text" class="form-control" id="judul" name="judul" value="<?php echo !empty($blog) ? $blog['judul'] : ''; ?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="alamat" class="col-sm-2 control-label">Foto</label>
                                <div class="col-sm-3">
                                    <?php echo $blog['foto'] ? '<img class="img-thumbnail" src="'.URL_USER."upload/".$blog['foto'].'"/><br><br>' : '' ?>
                                    <input name="foto" type="file" id="foto"> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="konten" class="col-sm-2 control-label">Konten</label>
                                <div class="col-sm-10">
                                    <div class="box box-info">
                                        <div class="box-body pad">
                                            <textarea id="konten" name="konten" class="textarea" <?php echo $blog['konten'] ? '' : 'placeholder="Place some text here"'; ?>
                                                    style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                                <?php echo !empty($blog) ? $blog['konten'] : ''; ?>
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12" style="text-align:right;">
                                    <a href="<?php echo URL_ADMIN."controller/blog/"?>" class="btn btn-danger">Cancel</a>
                                    <button type="reset" class="btn btn-warning">Reset</button>
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>