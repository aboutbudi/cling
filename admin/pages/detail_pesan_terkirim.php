<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pesan
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-envelope-o"></i> Home</a></li>
        <li class="active">Pesan</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Folders</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="<?php echo URL_ADMIN."controller/pesan/"?>"><i class="fa fa-inbox"></i> Inbox
                  <span class="label label-primary pull-right"><?php echo $jml_pesan['jumlah_pesan_masuk']>0 ? $jml_pesan['jumlah_pesan_masuk'] :'';?></span></a></li>
                <li><a href="<?php echo URL_ADMIN."controller/pesan/terkirim.php"?>"><i class="fa fa-envelope-o"></i> Sent 
                <span class="label label-primary pull-right"><?php echo $jml_sent['jumlah_pesan_terkirim']>0 ? $jml_sent['jumlah_pesan_terkirim'] :'';?></span></a></li>
                <li><a href="<?php echo URL_ADMIN."controller/pesan/semua.php"?>"><i class="fa fa-envelope-open-o"></i> Semua Pesan 
                <span class="label label-primary pull-right"><?php echo $jml_sent_all['jumlah_pesan_terkirim']>0 ? $jml_sent_all['jumlah_pesan_terkirim'] :'';?></span></a></li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Pesan Dari : <?php echo $pesan_masuk['nama_pelanggan']?></h3>
              <h5><span class="mailbox-read-time pull-right"><?php echo $pesan_masuk['waktu_pengiriman']?></span></h5>
              
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-read-info">
                <h3><?php echo "Subyek : ".$pesan_masuk['subyek']?></h3>
              </div>
              
              <!-- /.mailbox-read-info -->
              
              <!-- /.mailbox-controls -->
              <div class="mailbox-read-message">
                <?php echo $pesan_masuk['isi_pesan']?>
              </div>
              <div class="mailbox-read-info">
                <h3 class="box-title">Balasan Dari : <?php echo $pesan_masuk['nama_karyawan']?></h3>
              <h5><span class="mailbox-read-time pull-right"><?php echo $pesan_masuk['waktu_balas']?></span></h5>
              </div>
              
              <!-- /.mailbox-read-info -->
              
              <!-- /.mailbox-controls -->
              <div class="mailbox-read-message">
                <?php echo $pesan_masuk['isi_tanggapan']?>
              </div>
              <!-- /.mailbox-read-message -->
            </div>
            <!-- /.box-body -->
            
          </div>
          <!-- /. box -->
        </div>
    </section>
</div>