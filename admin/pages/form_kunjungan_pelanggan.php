<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Kujungan Pelanggan
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-xing"></i> Home</a></li>
        <li class="active">Kujungan Pelanggan</li><li class="active"><?php echo $kunjungan_pelanggan['id_kunjungan_pelanggan'] ? 'Ubah' : 'Tambah'; ?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header">
                        <a href="<?php echo URL_ADMIN."controller/kunjungan_pelanggan/"?>" class="pull-right btn btn-sm btn-danger" tooltip="view" alt="view"><i class="fa fa-backward"></i> Kembali</a>
                        <h3 class="box-title"><?php echo $kunjungan_pelanggan['id_kunjungan_pelanggan'] ? 'Ubah Kujungan Pelanggan' : 'Tambah Kujungan Pelanggan'; ?></h3>
                    </div>
                    <div class="box-body">
                        <form action="<?php echo $kunjungan_pelanggan['id_kunjungan_pelanggan'] ? URL_ADMIN."controller/kunjungan_pelanggan/ubah.php" : URL_ADMIN."controller/kunjungan_pelanggan/tambah.php"; ?>" method="post" class="form-horizontal">
                            <div class="form-group">
                                <label for="nama" class="col-sm-3 control-label">Nama Pelanggan</label>
                                <div class="col-sm-9">
                                    <input type="hidden" class="form-control" id="id_kunjungan_pelanggan" name="id_kunjungan_pelanggan" value="<?php echo $kunjungan_pelanggan['id_kunjungan_pelanggan'];?>">
                                    <select class="form-control select2" name="id_pelanggan" value="<?php echo !empty($kunjungan_pelanggan) ? $kunjungan_pelanggan['id_pelanggan'] : ''; ?>">
                                        <!-- <option <?php echo $kunjungan_pelanggan['id_pelanggan'] ? 'value="'.$kunjungan_pelanggan['id_pelanggan'].'" ' : 'value="1"'; ?> selected><?php echo $kunjungan_pelanggan['id_pelanggan'] ? $nama_pelanggan['nama_pelanggan'] : 'Member Umum'; ?> </option> -->
                                        <?php
                                            foreach($pelanggan as $pelanggan){
                                        ?>
                                        <option value="<?php echo $pelanggan['id_pelanggan']; ?>"><?php echo $pelanggan['nomor_pelanggan']." - ".$pelanggan['nama_pelanggan']; ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-12" style="text-align:right;">
                                    <a href="<?php echo URL_ADMIN."controller/kunjungan_pelanggan/"?>" class="btn btn-danger">Cancel</a>
                                    <button type="reset" class="btn btn-warning">Reset</button>
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>