<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Promo
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Promo</li><li class="active">Hapus</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header">
                        <a href="<?php echo URL_ADMIN."controller/promo/"?>" class="pull-right btn btn-sm btn-danger" tooltip="view" alt="view"><i class="fa fa-backward"></i> Kembali</a>
                        <h3 class="box-title">Hapus Promo</h3>
                    </div>
                    <div class="box-body">
                        <form enctype="multipart/form-data" action="<?php echo $promo['id_promo'] ? URL_ADMIN."controller/promo/hapus.php" : "" ?>" method="post" class="form-horizontal">
                            <div class="form-group">
                                <label for="nama_promo" class="col-sm-2 control-label">Promo</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nama_promo" name="nama_promo" value="<?php echo !empty($promo) ? $promo['nama_promo'] : ''; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nama" class="col-sm-2 control-label">Perawatan</label>
                                <div class="col-sm-10">
                                    <input type="hidden" class="form-control" id="id_promo" name="id_promo" value="<?php echo !empty($promo) ? $promo['id_promo'] : ''; ?>">
                                    <select class="form-control" name="id_perawatan" value="<?php echo !empty($promo) ? $promo['id_perawatan'] : ''; ?>" disabled>
                                        <option <?php echo $promo['id_perawatan'] ? 'value="'.$promo['id_perawatan'].'" ' : 'value="0"'; ?> selected>
                                        <?php
                                            if($promo['id_perawatan']){
                                                if($promo['id_perawatan']==0){
                                                    echo "Semua Perawatan";
                                                }else{
                                                    echo $nama_perawatan['nama_perawatan'];
                                                }
                                            }else{
                                                echo "Semua Perawatan";
                                            }
                                        ?> 
                                        </option>
                                        <?php
                                            foreach($perawatan as $perawatan){
                                        ?>
                                        <option value="<?php echo $perawatan['id_perawatan']; ?>"><?php echo $perawatan['nama_perawatan']; ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="deskripsi" class="col-sm-2 control-label">deskripsi</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="4" id="deskripsi" name="deskripsi" disabled><?php echo !empty($promo) ? $promo['deskripsi'] : ''; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="penggunaan_poin" class="col-sm-2 control-label">Penggunaan Poin</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" id="penggunaan_poin" name="penggunaan_poin" value="<?php echo !empty($promo) ? $promo['penggunaan_poin'] : ''; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="potongan" class="col-sm-2 control-label">Potongan Harga</label>
                                <label for="potongan" class="col-sm-2 control-label">Rp.</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" id="potongan" name="potongan" value="<?php echo !empty($promo) ? $promo['potongan'] : ''; ?>" disabled>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="tanggal_mulai" class="col-sm-2 control-label">Tanggal Mulai:</label>
                                <div class="col-sm-4">
                                    <div class="input-group date" data-provide="datepicker">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right datepicker" id="tanggal_selesai" name="tanggal_mulai" value="<?php echo !empty($promo) ? $promo['tanggal_mulai'] : ''; ?>" disabled>
                                    </div>
                                </div>

                                <label for="tanggal_selesai" class="col-sm-2 control-label">Tanggal Selesai:</label>
                                <div class="col-sm-4">
                                    <div class="input-group date" data-provide="datepicker">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right datepicker" data-date-format="yyyy/mm/dd" id="tanggal_selesai" name="tanggal_selesai" value="<?php echo !empty($promo) ? $promo['tanggal_selesai'] : ''; ?>" disabled>
                                    </div>
                                </div>                                
                                <!-- /.input group -->
                            </div>
                            <div class="form-group">
                                <label for="alamat" class="col-sm-2 control-label">Foto</label>
                                <div class="col-sm-3">
                                    <?php echo $promo['foto'] ? '<img class="img-thumbnail" src="'.URL_USER."upload/".$promo['foto'].'"/><br><br>' : '' ?>
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-10" style="color:red;text-align:right;">
                                    <h4><b>Apakah anda yakin ingin menghapus data ini ?<b></h4>
                                    
                                </div>
                                <div class="col-xs-2">
                                    <a href="<?php echo URL_ADMIN."controller/promo/"?>" class="btn btn-success">Cancel</a>
                                    <button type="submit" class="btn btn-danger">Hapus</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>