<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pesan
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-envelope-o"></i> Home</a></li>
        <li class="active">Pesan</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Folders</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="<?php echo URL_ADMIN."controller/pesan/"?>"><i class="fa fa-inbox"></i> Inbox
                  <span class="label label-primary pull-right"><?php echo $jml_pesan['jumlah_pesan_masuk']>0 ? $jml_pesan['jumlah_pesan_masuk'] :'';?></span></a></li>
                <li><a href="<?php echo URL_ADMIN."controller/pesan/terkirim.php"?>"><i class="fa fa-envelope-o"></i> Sent 
                <span class="label label-primary pull-right"><?php echo $jml_sent['jumlah_pesan_terkirim']>0 ? $jml_sent['jumlah_pesan_terkirim'] :'';?></span></a></li>
                <li><a href="<?php echo URL_ADMIN."controller/pesan/semua.php"?>"><i class="fa fa-envelope-open-o"></i> Semua Pesan 
                <span class="label label-primary pull-right"><?php echo $jml_sent_all['jumlah_pesan_terkirim']>0 ? $jml_sent_all['jumlah_pesan_terkirim'] :'';?></span></a></li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title">Semua Pesan</h3>

                    <!-- <div class="box-tools pull-right">
                        <div class="has-feedback">
                        <input type="text" class="form-control input-sm" placeholder="Search Mail">
                        <span class="glyphicon glyphicon-search form-control-feedback"></span>
                        </div>
                    </div> -->
                <!-- /.box-tools -->
                </div>
                <div class="box-body no-padding">
                    <div class="table-responsive mailbox-messages">
                        <table class="table table-hover table-striped">
                        <tbody>
                        <?php 
                        if (is_array($pesan_masuk) || is_object($pesan_masuk)){
                            foreach($pesan_masuk as $pesan_masuk)
                            {
                        ?>
                        <tr>
                            <td class="mailbox-star"><a href="#"><i class="fa fa-inbox text-yellow"></i></a></td>
                            <td class="mailbox-name"><a href="<?php echo URL_ADMIN."controller/pesan/detail.php?id_pesan=".$pesan_masuk['id_pesan']."&tipe=sent"?>"><?php echo "Dari : ".$pesan_masuk['nama_pelanggan']?></a></td>
                            <td class="mailbox-subject"><b><?php echo "Subyek : ".$pesan_masuk['subyek']?></b>
                            </td>
                            <td class="mailbox-date"><em><?php echo "dibalas pada : ".$pesan_masuk['waktu_balas']?></em></td>
                        </tr>
                        <?php 
                            }
                            }
                            if($jml_sent_all['jumlah_pesan_terkirim']==0){
                              echo '<tr><td colspan="4"><i class="fa fa-info-circle text-yellow"></i> No data Available</td></tr>';
                            }
                        ?>
                        </tbody>
                        </table>
                        <!-- /.table -->
                    </div>
                    <!-- /.mail-box-messages -->
                    </div>
                    <!-- /.box-body -->
            </div>
        </div>
    </section>
</div>