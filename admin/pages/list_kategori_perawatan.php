<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Kategori Perawatan
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-shower"></i> Home</a></li>
        <li class="active">Kategori Perawatan</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-info">
            <div class="box-header">
              <a href="<?php echo URL_ADMIN."controller/kategori_treatment/tambah.php"?>" class="pull-right btn btn-sm btn-success" tooltip="view" alt="view"><i class="fa fa-plus"></i> Tambah Kategori Perawatan</a>
              <h3 class="box-title">List Kategori Perawatan</h3>
            </div>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Kaegori Treatment</th>
                  <th>Deskripsi</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    <?php 
                        if (is_array($kategori_perawatan) || is_object($kategori_perawatan)){
                        foreach($kategori_perawatan as $kategori_perawatan)
                        {
                    ?>
                  <tr>
                    <td><?php echo $kategori_perawatan['nama'];?></td>
                    <td><?php echo $kategori_perawatan['deskripsi'];?></td>
                    <td>
                      <a href="<?php echo URL_ADMIN."controller/kategori_treatment/detail.php?id_kategori_perawatan=".$kategori_perawatan['id_kategori_perawatan']; ?>" class="btn btn-sm btn-info" tooltip="view" alt="view"><i class="fa fa-list"></i></a>
                      <a href="<?php echo URL_ADMIN."controller/kategori_treatment/ubah.php?id_kategori_perawatan=".$kategori_perawatan['id_kategori_perawatan']; ?>" class="btn btn-sm btn-warning" tooltip="view" alt="view"><i class="fa fa-pencil-square-o"></i></a>
                      <a href="<?php echo URL_ADMIN."controller/kategori_treatment/hapus.php?id_kategori_perawatan=".$kategori_perawatan['id_kategori_perawatan']; ?>" class="btn btn-sm btn-danger" tooltip="view" alt="view"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                  <?php 
                    }
                    }
                  ?>
                </tbody>
                
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
</div>