<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Perawatan
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Perawatan</li><li class="active"><?php echo $perawatan['id_perawatan'] ? 'Ubah' : 'Tambah'; ?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header">
                        <a href="<?php echo URL_ADMIN."controller/treatment/"?>" class="pull-right btn btn-sm btn-danger" tooltip="view" alt="view"><i class="fa fa-backward"></i> Kembali</a>
                        <h3 class="box-title"><?php echo $perawatan['id_perawatan'] ? 'Ubah Perawatan' : 'Tambah Perawatan'; ?></h3>
                    </div>
                    <div class="box-body">
                        <form enctype="multipart/form-data" action="<?php echo $perawatan['id_perawatan'] ? URL_ADMIN."controller/treatment/ubah.php" : URL_ADMIN."controller/treatment/tambah.php"; ?>" method="post" class="form-horizontal">
                            <div class="form-group">
                                <label for="nama" class="col-sm-2 control-label">Kategori Perawatan</label>
                                <div class="col-sm-10">
                                    <input type="hidden" class="form-control" id="id_perawatan" name="id_perawatan" value="<?php echo !empty($perawatan) ? $perawatan['id_perawatan'] : ''; ?>">
                                    <select class="form-control" name="id_kategori_perawatan" value="<?php echo !empty($perawatan) ? $perawatan['id_kategori_perawatan'] : ''; ?>">
                                        <option <?php echo $perawatan['id_kategori_perawatan'] ? 'value="'.$perawatan['id_kategori_perawatan'].'" ' : 'disabled'; ?> selected><?php echo $perawatan['id_kategori_perawatan'] ? $nama_kategori_perawatan['nama'] : 'Pilih Kategori Perawatan'; ?> </option>
                                        <?php
                                            foreach($kategori_perawatan as $kategori_perawatan){
                                        ?>
                                        <option value="<?php echo $kategori_perawatan['id_kategori_perawatan']; ?>"><?php echo $kategori_perawatan['nama']; ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nama_perawatan" class="col-sm-2 control-label">Nama Perawatan</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nama_perawatan" name="nama_perawatan" value="<?php echo !empty($perawatan) ? $perawatan['nama_perawatan'] : ''; ?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="fasilitas" class="col-sm-2 control-label">Fasilitas</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="fasilitas" name="fasilitas" value="<?php echo !empty($perawatan) ? $perawatan['fasilitas'] : ''; ?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="deskripsi" class="col-sm-2 control-label">deskripsi</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="4" id="deskripsi" name="deskripsi"><?php echo !empty($perawatan) ? $perawatan['deskripsi'] : ''; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="harga" class="col-sm-2 control-label">Harga</label>
                                <label for="harga" class="col-sm-2 control-label">Rp.</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" id="harga" name="harga" value="<?php echo !empty($perawatan) ? $perawatan['harga'] : ''; ?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="poin" class="col-sm-2 control-label">Poin</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" id="poin" name="poin" value="<?php echo !empty($perawatan) ? $perawatan['poin'] : ''; ?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="alamat" class="col-sm-2 control-label">Foto</label>
                                <div class="col-sm-3">
                                    <?php echo $perawatan['foto'] ? '<img class="img-thumbnail" src="'.URL_USER."upload/".$perawatan['foto'].'"/><br><br>' : '' ?>
                                    <input name="foto" type="file" id="foto"> 
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12" style="text-align:right;">
                                    <a href="<?php echo URL_ADMIN."controller/treatment/"?>" class="btn btn-danger">Cancel</a>
                                    <button type="reset" class="btn btn-warning">Reset</button>
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>