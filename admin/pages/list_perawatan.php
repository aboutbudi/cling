<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Perawatan
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-bath"></i> Home</a></li>
        <li class="active">Perawatan</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-info">
            <div class="box-header">
              <a href="<?php echo URL_ADMIN."controller/treatment/tambah.php"?>" class="pull-right btn btn-sm btn-success" tooltip="view" alt="view"><i class="fa fa-plus"></i> Tambah Perawatan</a>
              <h3 class="box-title">List Perawatan</h3>
            </div>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Kategori Treatment</th>
                  <th>Nama Perawatan</th>
                  <th>Harga</th>
                  <th>Poin</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    <?php 
                        if (is_array($perawatan) || is_object($perawatan)){
                        foreach($perawatan as $perawatan)
                        {
                    ?>
                  <tr>
                    <td><?php echo $perawatan['kategori_perawatan'];?></td>
                    <td><?php echo $perawatan['nama_perawatan'];?></td>
                    <td>Rp. <?php echo $perawatan['harga'];?></td>
                    <td><?php echo $perawatan['poin'];?></td>
                    <td>
                      <a href="<?php echo URL_ADMIN."controller/treatment/detail.php?id_perawatan=".$perawatan['id_perawatan']; ?>" class="btn btn-sm btn-info" tooltip="view" alt="view"><i class="fa fa-list"></i></a>
                      <a href="<?php echo URL_ADMIN."controller/treatment/ubah.php?id_perawatan=".$perawatan['id_perawatan']; ?>" class="btn btn-sm btn-warning" tooltip="view" alt="view"><i class="fa fa-pencil-square-o"></i></a>
                      <a href="<?php echo URL_ADMIN."controller/treatment/hapus.php?id_perawatan=".$perawatan['id_perawatan']; ?>" class="btn btn-sm btn-danger" tooltip="view" alt="view"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                  <?php 
                    }
                    }
                  ?>
                </tbody>
                
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
</div>