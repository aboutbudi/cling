<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Blog
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-wordpress"></i> Home</a></li>
        <li class="active">Blog</li><li class="active">Detail</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header">
                        <a href="<?php echo URL_ADMIN."controller/blog/"?>" class="pull-right btn btn-sm btn-danger" tooltip="view" alt="view"><i class="fa fa-backward"></i> Kembali</a>
                        <h3 class="box-title">Detail Posting</h3>
                    </div>
                    <div class="box-body">
                        <div class="box box-widget">
                            <div class="box-header with-border">
                                <div class="user-block">
                                    <img class="img-circle" src="<?php echo URL_USER ?>img/user3-128x128.jpg" alt="User Image">
                                    <span class="username"><a href="#"><?php echo $blog['judul'] ?></a></span>
                                    <span class="description"> Penulis : <?php echo $blog['penulis'] ?> | Tanggal Posting - <?php echo $blog['tanggal_posting'] ?></span>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div>
                                    <?php echo $blog['konten'] ?>
                                </div>
                                <span class="pull-right text-muted">0 likes - 0 comments</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>