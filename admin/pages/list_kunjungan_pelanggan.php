<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Kunjungan Pelanggan
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-xing"></i> Home</a></li>
        <li class="active">Kunjungan Pelanggan</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-info">
            <div class="box-header">
              <a href="<?php echo URL_ADMIN."controller/kunjungan_pelanggan/tambah.php"?>" class="pull-right btn btn-sm btn-success" tooltip="view" alt="view"><i class="fa fa-plus"></i> Tambah Kunjungan</a>
              <h3 class="box-title">List Kunjungan Pelanggan</h3>
            </div>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Taggal Kunjungan</th>
                  <th>Nomor Kunjungan</th>
                  <th>Pelanggan</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    <?php 
                        if (is_array($kunjungan_pelanggan) || is_object($kunjungan_pelanggan)){
                        foreach($kunjungan_pelanggan as $kunjungan_pelanggan)
                        {
                    ?>
                  <tr>
                    <td><?php echo $kunjungan_pelanggan['tanggal_kunjungan'];?></td>
                    <td><?php echo $kunjungan_pelanggan['nomor_kunjungan'];?></td>
                    <td><?php echo $kunjungan_pelanggan['nomor_pelanggan']." - ".$kunjungan_pelanggan['nama_pelanggan'];?></td>
                    <td>
                    <?php
                      if(!($kunjungan_pelanggan['status_transaksi'])){
                        echo '<span class="label label-danger">Belum Transaksi</span>';
                      }elseif($kunjungan_pelanggan['status_transaksi']==1){
                        echo '<span class="label label-warning">Belum Checkout</span>';
                      }elseif($kunjungan_pelanggan['status_transaksi']==2){
                        echo '<span class="label label-info">Belum Bayar</span>';
                      }elseif($kunjungan_pelanggan['status_transaksi']==3){
                        echo '<span class="label label-success">Lunas</span>';
                      }
                      ?>
                    </td>
                    <td>
                      <a href="<?php echo URL_ADMIN."controller/transaksi_pelanggan/index.php?id_kunjungan_pelanggan=".$kunjungan_pelanggan['id_kunjungan_pelanggan']; ?>" class="btn btn-sm btn-info" tooltip="view" alt="view"><i class="fa fa-gear"></i> Lihat Transaksi</a>
                    </td>
                  </tr>
                  <?php 
                    }
                    }
                  ?>
                </tbody>
                
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">List Kunjungan Pelanggan Complete</h3>
            </div>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Taggal Kunjungan</th>
                  <th>Nomor Kunjungan</th>
                  <th>Pelanggan</th>
                  <th>Status</th>
                </tr>
                </thead>
                <tbody>
                    <?php 
                        if (is_array($semua_kunjungan_pelanggan) || is_object($semua_kunjungan_pelanggan)){
                        foreach($semua_kunjungan_pelanggan as $semua_kunjungan_pelanggan)
                        {
                    ?>
                  <tr>
                    <td><?php echo $semua_kunjungan_pelanggan['tanggal_kunjungan'];?></td>
                    <td><?php echo $semua_kunjungan_pelanggan['nomor_kunjungan'];?></td>
                    <td><?php echo $semua_kunjungan_pelanggan['nomor_pelanggan']." - ".$semua_kunjungan_pelanggan['nama_pelanggan'];?></td>
                    <td>
                      <a href="<?php echo URL_ADMIN."controller/transaksi_pelanggan/index.php?id_kunjungan_pelanggan=".$semua_kunjungan_pelanggan['id_kunjungan_pelanggan']; ?>">
                        <?php
                        if(!($semua_kunjungan_pelanggan['status_transaksi'])){
                          echo '<span class="label label-danger">Belum Transaksi</span>';
                        }elseif($semua_kunjungan_pelanggan['status_transaksi']==1){
                          echo '<span class="label label-warning">Belum Checkout</span>';
                        }elseif($semua_kunjungan_pelanggan['status_transaksi']==2){
                          echo '<span class="label label-info">Belum Bayar</span>';
                        }elseif($semua_kunjungan_pelanggan['status_transaksi']==3){
                          echo '<span class="label label-success">Lunas</span>';
                        }
                        ?>
                      </a>
                    </td>
                  </tr>
                  <?php 
                    }
                    }
                  ?>
                </tbody>
                
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
</div>