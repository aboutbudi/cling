<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Member
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Member</li><li class="active">Detail</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header">
                        <a href="<?php echo URL_ADMIN."controller/member/"?>" class="pull-right btn btn-sm btn-danger" tooltip="view" alt="view"><i class="fa fa-backward"></i> Kembali</a>
                        <h3 class="box-title">Detail Member</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-9">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label for="nomor_pelanggan" class="col-sm-3 control-label">Nomor Pelanggan</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="nomor_pelanggan" value="<?php echo $pelanggan_detail["nomor_pelanggan"];?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nomor_pelanggan" class="col-sm-3 control-label">Nama Pelanggan</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="nomor_pelanggan" value="<?php echo $pelanggan_detail["nama_pelanggan"];?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nomor_pelanggan" class="col-sm-3 control-label">Email</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="nomor_pelanggan" value="<?php echo $pelanggan_detail["email"];?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nomor_pelanggan" class="col-sm-3 control-label">Nomor Telepon</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="nomor_pelanggan" value="<?php echo $pelanggan_detail["nomor_telepon"];?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nomor_pelanggan" class="col-sm-3 control-label">Alamat</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="nomor_pelanggan" value="<?php echo $pelanggan_detail["alamat"];?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nomor_pelanggan" class="col-sm-3 control-label">Member Sejak</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="nomor_pelanggan" value="<?php echo $pelanggan_detail["tanggal_mendaftar"];?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nomor_pelanggan" class="col-sm-3 control-label">Jumlah Poin</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="nomor_pelanggan" value="<?php echo $pelanggan_detail["jumlah_poin"];?>" disabled>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-xs-3">
                                <img class="img-thumbnail" src="<?php echo $pelanggan_detail['foto'] ? URL_USER."upload/".$pelanggan_detail['foto'] : ''; ?>"/>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>