<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Detail Pembayaran
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-shopping-bag"></i> Home</a></li>
        <li class="active">Pembayaran</li><li class="active">Detail Pembayaran</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-lg-9 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3><?php echo $pembayaran['nomor_pelanggan']." - ".$pembayaran['nama_pelanggan'];?></h3>

                        <h4><?php echo $pembayaran['nomor_kunjungan']." - ".$pembayaran['tanggal_checkout']?></h4>
                    </div>
                    <div class="icon" style="margin-top:15px;">
                        <i class="fa fa-user-o"></i>
                    </div>
                    <a href="#" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h4><?php echo "Rp. ".$jumlah['total_biaya']?></h4>
                        <p><?php echo "Poin issue : ".$jumlah['total_poin']?></p>
                    </div>
                    <div class="icon"style="margin-top:15px;">
                        <i class="fa fa-shopping-basket"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                    <i class="fa fa-arrow-circle-down"></i>
                    </a>
                </div>
            </div>
            
        </div>
        <div clas="row">
        <a href="<?php echo URL_ADMIN."controller/pembayaran/" ?>" class="btn btn-danger" style="float:right;"><i class="fa fa-backward"></i> Kembali</a>
        <br><br>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">List Transaksi</h3>
                    </div>
                    <div class="box-body">
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tr>
                                    <th>Waktu Transaksi</th>
                                    <th>Transaksi</th>
                                    <th>Poin</th>
                                    <th>Biaya</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                                <?php 
                                    if (is_array($list_transaksi) || is_object($list_transaksi)){
                                    foreach($list_transaksi as &$transaksi)
                                    {
                                ?>
                                <tr>
                                    <td><?php echo $transaksi['tanggal_transaksi']?></td>
                                    <td><?php echo $transaksi['nama_perawatan']?></td>
                                    <td><?php echo $transaksi['poin']?></td>
                                    <td style="text-align:right;"><?php echo "Rp. ".$transaksi['biaya']?></td>
                                    
                                    <?php
                                        if($transaksi['status_transaksi']==1){
                                            echo '<td><span class="label label-info">Belum Checkout</span></td>
                                            <td><a href="'.URL_ADMIN.'controller/transaksi_pelanggan/hapus.php?id_kunjungan='.$id_kunjungan_pelanggan.'&id_transaksi_kunjungan='.$transaksi['id_transaksi_kunjungan'].'"><span class="label label-danger">delete</span></a></td>';
                                        }elseif($transaksi['status_transaksi']==2){
                                            echo "<td><span class=\"label label-warning\">Belum Bayar</span></td>";
                                        }elseif($transaksi['status_transaksi']==3){
                                            echo "<td><span class=\"label label-success\">Lunas</span></td>";
                                        }
                                    ?>

                                    
                                </tr>
                                <?php }
                                unset($transaksi);
                                }?>
                                <tr>
                                <th colspan="2" style="text-align:right;">Total</th>
                                <th><?php echo $jumlah['total_poin']?></th>
                                <th style="text-align:right;"><?php echo "Rp. ".$jumlah['total_biaya']?></th>
                                </tr>
                                <tr>
                                <th colspan="2" style="text-align:right;">Diskon</th>
                                <th></th>
                                <th style="text-align:right;"><?php echo "Rp. ".$voucher['potongan']?></th>
                                </tr>
                                <tr>
                                <th colspan="2" style="text-align:right;">Harga Akhir</th>
                                <th></th>
                                <th style="text-align:right;"><?php echo "Rp. ".$jumlah['total_biaya']?></th>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="box-footer">
                    <!-- <div class="row">
                        <form action="<?php echo URL_ADMIN."controller/pembayaran/detail.php" ?>" method="post">
                            <div class="form-group">
                                <label for="kode_voucher" class="col-sm-2 control-label">Masukan Kode voucher</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" id="kode_voucher" name="kode_voucher" >
                                </div>
                                <div class="col-sm-3">
                                    <button type="btn" class="btn btn-primary">
                                        <i class="fa fa-shopping-cart"></i> Pakai Voucher
                                    </button>
                                </div>
                            </div>
                        </form> -->
                        <?php
                            if($pembayaran['status_pembayaran']==2){
                            ?>
                            <form action="<?php echo URL_ADMIN."controller/pembayaran/bayar.php" ?>" method="post">
                            <input type="hidden" class="form-control" id="id_pembayaran" name="id_pembayaran" value="<?php echo $pembayaran['id_pembayaran']?>">
                            <input type="hidden" class="form-control" id="total_biaya" name="total_biaya" value="<?php echo $jumlah['total_biaya']?>">
                            <input type="hidden" class="form-control" id="total_poin" name="total_poin" value="<?php echo $jumlah['total_poin']?>">
                            <button type="submit" class="btn btn-success">
                                <i class="fa fa-shopping-cart"></i> Bayar
                            </button>
                            </form>
                            <?php
                            }
                        ?>
                    </div>
                    
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="tambah-transaksi">
        <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?php echo URL_ADMIN."controller/transaksi_pelanggan/tambah.php" ?>" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo "Tambah Transaksi ".$kunjungan_pelanggan['nomor_kunjungan']." - ".$kunjungan_pelanggan['nama_pelanggan']?></h4>
                </div>
                <div class="modal-body">
                   <div class="form-group">
                        <label for="nama" class="col-md-2 control-label">Perawatan</label>
                        <div class="col-md-10">
                            <input type="hidden" class="form-control" id="id_kunjungan" name="id_kunjungan" value="<?php echo $id_kunjungan_pelanggan; ?>">
                            <select class="select2" name="id_perawatan" value="">
                                    <?php
                                        foreach($perawatan as $perawatan){
                                    ?>
                                    <option value="<?php echo $perawatan['id_perawatan']; ?>"><?php echo $perawatan['nama_perawatan']; ?></option>
                                    <?php
                                        }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <br><br>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="checkout">
        <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?php echo URL_ADMIN."controller/transaksi_pelanggan/checkout.php" ?>" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo "Proses Checkout ".$kunjungan_pelanggan['nomor_kunjungan']." - ".$kunjungan_pelanggan['nama_pelanggan']?></h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive no-padding">
                            <table class="table table-hover">
                                <tr>
                                    <th>Transaksi</th>
                                    <th>Poin</th>
                                    <th>Biaya</th>
                                    <th>Status</th>
                                </tr>
                                <?php 
                                    if (is_array($list_transaksi) || is_object($list_transaksi)){
                                    foreach($list_transaksi as $list_transaksi)
                                    {
                                ?>
                                <tr>
                                    <td><?php echo $list_transaksi['nama_perawatan']?></td>
                                    <td><?php echo $list_transaksi['poin']?></td>
                                    <td style="text-align:right;"><?php echo "Rp. ".$list_transaksi['biaya']?></td>
                                    
                                    <?php
                                        if($list_transaksi['status_transaksi']==1){
                                            echo '<td><span class="label label-info">Belum Checkout</span></td>';
                                        }elseif($list_transaksi['status_transaksi']==2){
                                            echo "<td><span class=\"label label-warning\">Belum Bayar</span></td>";
                                        }elseif($list_transaksi['status_transaksi']==3){
                                            echo "<td><span class=\"label label-success\">Lunas</span></td>";
                                        }
                                    ?>
                                    
                                </tr>
                                <?php }}?>
                                <tr>
                                <th style="text-align:right;">Total</th>
                                <th><?php echo $jumlah['total_poin']?></th>
                                <th style="text-align:right;"><?php echo "Rp. ".$jumlah['total_biaya']?></th>
                                </tr>
                            </table>
                        </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" class="form-control" id="id_kunjungan" name="id_kunjungan" value="<?php echo $id_kunjungan_pelanggan; ?>">
                    <input type="hidden" class="form-control" id="total_harga" name="total_harga" value="<?php echo $jumlah['total_biaya']; ?>">
                    <input type="hidden" class="form-control" id="total_poin" name="total_poin" value="<?php echo $jumlah['total_poin']; ?>">
                    <button type="submit" class="btn btn-success"><i class="fa fa-shopping-cart"></i> Checkout</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>