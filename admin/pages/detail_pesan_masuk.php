<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pesan
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-envelope-o"></i> Home</a></li>
        <li class="active">Pesan</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Folders</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="<?php echo URL_ADMIN."controller/pesan/"?>"><i class="fa fa-inbox"></i> Inbox
                  <span class="label label-primary pull-right"><?php echo $jml_pesan['jumlah_pesan_masuk']>0 ? $jml_pesan['jumlah_pesan_masuk'] :'';?></span></a></li>
                <li><a href="<?php echo URL_ADMIN."controller/pesan/terkirim.php"?>"><i class="fa fa-envelope-o"></i> Sent 
                <span class="label label-primary pull-right"><?php echo $jml_sent['jumlah_pesan_terkirim']>0 ? $jml_sent['jumlah_pesan_terkirim'] :'';?></span></a></li>
                <li><a href="<?php echo URL_ADMIN."controller/pesan/semua.php"?>"><i class="fa fa-envelope-open-o"></i> Semua Pesan 
                <span class="label label-primary pull-right"><?php echo $jml_sent_all['jumlah_pesan_terkirim']>0 ? $jml_sent_all['jumlah_pesan_terkirim'] :'';?></span></a></li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Pesan Dari : <?php echo $pesan_masuk['nama_pelanggan']?></h3>
              <h5><span class="mailbox-read-time pull-right"><?php echo $pesan_masuk['waktu_pengiriman']?></span></h5>
              
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-read-info">
                <h3><?php echo "Subyek : ".$pesan_masuk['subyek']?></h3>
              </div>
              
              <!-- /.mailbox-read-info -->
              
              <!-- /.mailbox-controls -->
              <div class="mailbox-read-message">
                <?php echo $pesan_masuk['isi_pesan']?>
              </div>
              <!-- /.mailbox-read-message -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <?php echo !($pesan_masuk['id_penanggap']) ? '<a href="#" class="btn btn-primary pull-right margin-bottom" data-toggle="modal" data-target="#balas-pesan">balas</a>':""?>
            </div>
            
          </div>
          <!-- /. box -->
        </div>
    </section>
</div>
<div class="modal fade" id="balas-pesan">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <form action="<?php echo URL_ADMIN."controller/pesan/balas.php" ?>" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Balasan untuk : <?php echo $pesan_masuk['nama_pelanggan']." - ".$pesan_masuk['subyek']?></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="form-group">
                        <textarea id="compose-textarea" name="isi_tanggapan"class="form-control textarea" style="height: 300px">
                        </textarea>
                    </div>
                </div>
                <br><br>
            </div>
            <div class="modal-footer">
            <input type="hidden" class="form-control" id="id_pesan" name="id_pesan" value="<?php echo $pesan_masuk['id_pesan']; ?>">
                <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Balas</button>
            </div>
        </form>
    </div>
  <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>