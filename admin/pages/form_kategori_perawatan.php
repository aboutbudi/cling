<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Kategori Perawatan
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Kategori Perawatan</li><li class="active"><?php echo $kategori_perawatan['id_kategori_perawatan'] ? 'Ubah' : 'Tambah'; ?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header">
                        <a href="<?php echo URL_ADMIN."controller/kategori_treatment/"?>" class="pull-right btn btn-sm btn-danger" tooltip="view" alt="view"><i class="fa fa-backward"></i> Kembali</a>
                        <h3 class="box-title"><?php echo $kategori_perawatan['id_kategori_perawatan'] ? 'Ubah Kategori Perawatan' : 'Tambah Kategori Perawatan'; ?></h3>
                    </div>
                    <div class="box-body">
                        <form action="<?php echo $kategori_perawatan['id_kategori_perawatan'] ? URL_ADMIN."controller/kategori_treatment/ubah.php" : URL_ADMIN."controller/kategori_treatment/tambah.php"; ?>" method="post" class="form-horizontal">
                            <div class="form-group">
                                <label for="nama" class="col-sm-3 control-label">Nama Kategori Perawatan</label>
                                <div class="col-sm-9">
                                    <input type="hidden" class="form-control" id="id_kategori_perawatan" name="id_kategori_perawatan" value="<?php echo !empty($kategori_perawatan) ? $kategori_perawatan['id_kategori_perawatan'] : ''; ?>">
                                    <input type="text" class="form-control" id="nama" name="nama" value="<?php echo !empty($kategori_perawatan) ? $kategori_perawatan['nama'] : ''; ?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="deskripsi" class="col-sm-3 control-label">deskripsi</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" rows="4" id="deskripsi" name="deskripsi"><?php echo !empty($kategori_perawatan) ? $kategori_perawatan['deskripsi'] : ''; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12" style="text-align:right;">
                                    <a href="<?php echo URL_ADMIN."controller/kategori_treatment/"?>" class="btn btn-danger">Cancel</a>
                                    <button type="reset" class="btn btn-warning">Reset</button>
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>