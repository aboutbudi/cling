<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Promo
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-gift"></i> Home</a></li>
        <li class="active">Promo</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-info">
            <div class="box-header">
              <a href="<?php echo URL_ADMIN."controller/promo/tambah.php"?>" class="pull-right btn btn-sm btn-success" tooltip="view" alt="view"><i class="fa fa-plus"></i> Tambah Promo</a>
              <h3 class="box-title">List Promo</h3>
            </div>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Promo</th>
                  <th>Perawatan</th>
                  <th>Poin</th>
                  <th>Potongan</th>
                  <th>Periode</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    <?php 
                        if (is_array($promo) || is_object($promo)){
                        foreach($promo as $promo)
                        {
                    ?>
                  <tr>
                    <td><?php echo $promo['nama_promo'];?></td>
                    <td><?php echo $promo['nama_perawatan'] ? $promo['nama_perawatan'] : "Semua Perawatan";?></td>
                    <td><?php echo $promo['penggunaan_poin'];?></td>
                    <td>Rp. <?php echo $promo['potongan'];?></td>
                    <td><?php echo $promo['tanggal_mulai']." - ".$promo['tanggal_selesai'];?></td>
                    <td>
                      <a href="<?php echo URL_ADMIN."controller/promo/detail.php?id_promo=".$promo['id_promo']; ?>" class="btn btn-sm btn-info" tooltip="view" alt="view"><i class="fa fa-list"></i></a>
                      <a href="<?php echo URL_ADMIN."controller/promo/ubah.php?id_promo=".$promo['id_promo']; ?>" class="btn btn-sm btn-warning" tooltip="view" alt="view"><i class="fa fa-pencil-square-o"></i></a>
                      <a href="<?php echo URL_ADMIN."controller/promo/hapus.php?id_promo=".$promo['id_promo']; ?>" class="btn btn-sm btn-danger" tooltip="view" alt="view"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                  <?php 
                    }
                    }
                  ?>
                </tbody>
                
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
</div>