<?php
	include("koneksi.php");
	//select 
	
	function select($table, $columns=null, $conditions=null){
		$koneksi = connect();
		$cols = ""; $cond = ""; $data = array();
		
		if($columns == null){
			$cols = '*';
		}else if(is_array($columns)){
			$cols = implode(', ', $columns);
		}else{
			$cols = $columns;
		}
		
		if($conditions != null ){
			$cond = " WHERE ".$conditions;
		}
		
		$sql = "SELECT ".$cols." FROM ".$table.$cond;
		
		$query = mysqli_query($koneksi, $sql) or die (mysqli_error());

		$data = mysqli_fetch_all($query,MYSQLI_ASSOC);
		
		return $data;
	}
	
	//insert
	function insert($table, $values, $columns=null){
		$koneksi = connect();
		$cols = ""; $vals = "";
		
		if(is_array($columns)){
			$cols = " (".implode(', ', $columns).")";
		}
				
		if(is_array($values)){
			if($columns == null){
				$vals = " VALUES (null,'".implode(' \', \' ', $values)."')";
			}else{
				$vals = " VALUES ('".implode('\', \'', $values)."')";
			}
		}else{
			if($columns == null){
				$vals = " VALUES (null,'".$values."')";
			}
		}
		$sql = "INSERT INTO ".$table.$cols.$vals;
		
		mysqli_query($koneksi, $sql) or die (mysqli_error($koneksi));
	}
	
	
	//update
	function update($table, $columns, $conditions=null){
		$koneksi = connect();
		$cond = ""; 
		$fields = "";
		
		if(is_array($columns)){
			$i=0;
			foreach($columns as $key => $val){
				$sparator = ($i == count($columns)-1) ? ' ' : ', ';
				$fields .= $key.' = "'.$val.'" '.$sparator;
				$i++;
			}
		}
		
		if($conditions != null ){
			$cond = " WHERE ".$conditions;
		}
		$sql = "UPDATE ".$table." SET ".$fields.$cond;
		
		mysqli_query($koneksi, $sql) or die (mysqli_error());
	}
	
	
	//delete
	function delete($table, $conditions=null){
		$koneksi = connect();
		$cond = " WHERE 1";
		
		if($conditions != null ){
			$cond = " WHERE ".$conditions;
		}
		$sql = "DELETE FROM ".$table.$cond;
		mysqli_query($koneksi, $sql) or die (mysqli_error());
	}

	
	//login
	function login($username, $password){
		$condition = 'username = "'.$username.'" AND password = "'.$password.'" ';
		$data = select('user', null, $condition);
		
		if(count($data) > 0)
		{
			session_start();
			if(!empty($_SESSION['user'])){
				session_destroy();
			}
			$_SESSION['user'] = $data[0];
		}
	}
	
	
	//logout
	function logout(){
		session_start();
		if(!empty($_SESSION['user'])){
			session_destroy();
		}
	}
	
	
	//select by sql
	function selectBySql($sql){
		$data = array();
		$koneksi = connect();
		$query = mysqli_query($koneksi, $sql) or die (mysqli_error());
		$data = mysqli_fetch_all($query,MYSQLI_ASSOC);
		
		return $data;
	}
	function selectCount($sql){
		$data = 0;
		$koneksi = connect();		
		$result = $koneksi->query($sql);
		$data = $result->fetch_assoc();
		
		return $data;
	}
	function selectDetail($sql){
		$data = 0;
		$koneksi = connect();		
		$result = $koneksi->query($sql);
		$data = $result->fetch_assoc();
		
		return $data;
	}
	
	
	//selectJoin
	function selectJoin($table, $relations, $fields=null, $condition=null){
		$koneksi = connect();
		$cols = ' * ';
		$foreign = " ";
		$cond = " ";
		
		if(is_array($relations)){
			foreach($relations as $key => $val){
				$foreign .= $val[1]." JOIN ".$key." ON ".$key.".".$val[0]." = ".$table.".".$val[0]." ";
			}
		}
		
		if(!empty($fields) && is_array($fields)){
			$cols = implode(', ', $fields);
		}
		
		if(!empty($condition)){
			$cond = " WHERE ".$condition;
		}
				
		$sql = "SELECT  ".$cols." FROM ".$table
				.$foreign
				.$cond;
		$query = mysqli_query($koneksi, $sql) or die (mysqli_error());
		$data = mysqli_fetch_all($query,MYSQLI_ASSOC);
		
		return $data;
	}
	
	
	//selectJoin2 tanpa where
	function selectJoin2($table, $relations, $fields=null, $condition=null){
		$koneksi = connect();
		$cols = ' * ';
		$foreign = " ";
		$cond = " ";
		
		if(is_array($relations)){
			foreach($relations as $key => $val){
				$foreign .= $val[1]." JOIN ".$key." ON ".$key.".".$val[0]." = ".$table.".".$val[0]." ";
			}
		}
		
		if(!empty($fields) && is_array($fields)){
			$cols = implode(', ', $fields);
		}
		
		if(!empty($condition)){
			$cond = $condition;
		}
				
		$sql = "SELECT  ".$cols." FROM ".$table
				.$foreign
				.$cond;
		$query = mysqli_query($koneksi, $sql) or die (mysqli_error());
		$data = mysqli_fetch_all($query,MYSQLI_ASSOC);
		
		return $data;
	}
	
	
	//generate code format PKT001 ATAU P00001 (TIDAK LEBIH DARI 6)
	function generateCode($default, $fields){
		$koneksi = connect();
		if(is_array($fields)){
			$prefix = substr($default, 0, 3);
			$sql = "SELECT MAX(".$fields[1].") AS kode FROM ".$fields[0];
			$query = mysqli_query($koneksi, $sql) or die (mysqli_error());
			$data = mysqli_fetch_all($query,MYSQLI_ASSOC);
			if(!empty($data) || count($data) > 0){
				$kode = $data[0]['kode'];
				if(!empty($kode)){
					$prefix = substr($kode, 0, 3);
					$number = (int) substr($kode, 3, 3);
					$number++;
					$result = $prefix.sprintf("%03s", $number);
					
					return $result;
				}else{
					return $default;
				}
			}
		}
	}
	
	function getLastId($table, $field){
		$koneksi = connect();
		$id = 1;
		$sql = "SELECT ".$field." as id FROM ".$table." ORDER BY ".$field." DESC LIMIT 1";
		$query = mysqli_query($koneksi, $sql) or die (mysqli_error());
		$data = mysqli_fetch_all($query,MYSQLI_ASSOC);
		if(!empty($data) || count($data) > 0){
			$id = $data[0]['id'];
		}
		
		return $id;
	}
	
	function crypto_rand_secure($min, $max)
	{
		$range = $max - $min;
		if ($range < 1) return $min; // not so random...
		$log = ceil(log($range, 2));
		$bytes = (int) ($log / 8) + 1; // length in bytes
		$bits = (int) $log + 1; // length in bits
		$filter = (int) (1 << $bits) - 1; // set all lower bits to 1
		do {
			$rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
			$rnd = $rnd & $filter; // discard irrelevant bits
		} while ($rnd > $range);
		return $min + $rnd;
	}

	function getCodeVoucher($length)
	{
		$token = "";
		$codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
		$codeAlphabet.= "0123456789";
		$max = strlen($codeAlphabet); // edited

		for ($i=0; $i < $length; $i++) {
			$token .= $codeAlphabet[crypto_rand_secure(0, $max-1)];
		}

		return $token;
	}
?>