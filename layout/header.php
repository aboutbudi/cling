<!-- banner -->
<div class="banner page-head">
	<div class="container">
		<div class="header">
			<div class="logo">
				<h1>
					<a class="link link--yaku" href="<?php echo URL_USER ?>">
						<span>C</span><span>L</span><span>I</span><span>N</span><span>G</span>					
					</a>
				</h1>
			</div>
			<div class="navigation">
				<span class="menu"><img src="<?php echo URL_USER ?>images/menu.png" alt=""/></span>
				<ul class="nav1">
					<li><a class="hvr-overline-from-center button2 <?php echo PAGE_LOCATION == 'about' ? 'active':''?>" href="<?php echo URL_USER ?>pages/about/">Tentang Kami</a></li>
                    <li><a class="hvr-overline-from-center button2 <?php echo PAGE_LOCATION == 'treatment' ? 'active':''?>" href="<?php echo URL_USER ?>pages/treatment/">Perawatan</a></li>
                    <li><a class="hvr-overline-from-center button2 <?php echo PAGE_LOCATION == 'blog' ? 'active':''?>" href="<?php echo URL_USER ?>pages/blog/">Blog</a></li>
					<li><a class="hvr-overline-from-center button2 <?php echo PAGE_LOCATION == 'member' ? 'active':''?>" href="<?php echo URL_USER ?>pages/member/">Membership</a></li>
					<li><a class="hvr-overline-from-center button2 <?php echo PAGE_LOCATION == 'kontak' ? 'active':''?>" href="<?php echo URL_USER ?>pages/kontak/">Kontak</a></li>
				</ul>
				<!-- script for menu -->
					<script> 
						$( "span.menu" ).click(function() {
						$( "ul.nav1" ).slideToggle( 300, function() {
						 // Animation complete.
						});
						});
					</script>
				<!-- //script for menu -->
			</div>
			<div class="search">
				<form>
					<input type="search" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}" required="">
					<input type="submit" value=" ">
				</form>
			</div>
			<div class="clearfix"></div>
        </div>
        <?php
        if(PAGE_LOCATION == 'home'){
            include 'banner.php';
        }
        ?>
	</div>
</div>
<!-- //banner -->