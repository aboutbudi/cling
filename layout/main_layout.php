<html>
    <head>
        <title><?php echo 'Cling SkinCare | '.PAGE_TITLE;?></title>
        <!-- for-mobile-apps -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Cling SkinCare, Cling, Facials, Skin Treatment, Smartphone Compatible web template" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
                function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/font-awesome/css/font-awesome.min.css">
        <!-- //for-mobile-apps -->
        <link href="<?php echo URL_USER ?>css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo URL_USER ?>css/style.css" rel="stylesheet" type="text/css" media="all" />
        <!-- js -->
        <script src="<?php echo URL_USER ?>js/jquery-1.11.1.min.js"></script>
        <!-- //js -->
        <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
            <!-- start-smoth-scrolling -->
                <script type="text/javascript" src="<?php echo URL_USER ?>js/move-top.js"></script>
                <script type="text/javascript" src="<?php echo URL_USER ?>js/easing.js"></script>
                <script type="text/javascript">
                    jQuery(document).ready(function($) {
                        $(".scroll").click(function(event){		
                            event.preventDefault();
                            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
                        });
                    });
                </script>
            <!-- start-smoth-scrolling -->
    </head>
<body>
    <?php
        include 'header.php';
        include $content_page;
        include 'bottom_navigation.php';
        include 'footer.php';
    ?>
<!-- for bootstrap working -->
	<script src="<?php echo URL_USER ?>js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- smooth scrolling -->
	<script type="text/javascript">
		$(document).ready(function() {
		/*
			var defaults = {
			containerID: 'toTop', // fading element id
			containerHoverID: 'toTopHover', // fading element hover id
			scrollSpeed: 1200,
			easingType: 'linear' 
			};
		*/								
		$().UItoTop({ easingType: 'easeOutQuart' });
		});
	</script>
	<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- //smooth scrolling -->
</body>
</html>