-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: cling_db
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `blog`
--

DROP TABLE IF EXISTS `blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog` (
  `id_blog` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(200) DEFAULT NULL,
  `konten` text,
  `foto` varchar(200) DEFAULT NULL,
  `tanggal_post` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `penulis` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_blog`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog`
--

LOCK TABLES `blog` WRITE;
/*!40000 ALTER TABLE `blog` DISABLE KEYS */;
INSERT INTO `blog` VALUES (1,'Tes Posting Blog','<p>Ini hanyalah test untuk posting Blog</p>','blog-img_zGb6NEk.jpg','2017-07-27 15:40:30','Mr. Little');
/*!40000 ALTER TABLE `blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `karyawan`
--

DROP TABLE IF EXISTS `karyawan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `karyawan` (
  `id_karyawan` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `nama_karyawan` varchar(100) DEFAULT NULL,
  `jabatan` varchar(45) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `telepon` varchar(15) DEFAULT NULL,
  `alamat` varchar(500) DEFAULT NULL,
  `foto` varchar(500) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_karyawan`),
  KEY `fk_karyawan_to_users_idx` (`id_user`),
  CONSTRAINT `fk_karyawan_to_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `karyawan`
--

LOCK TABLES `karyawan` WRITE;
/*!40000 ALTER TABLE `karyawan` DISABLE KEYS */;
INSERT INTO `karyawan` VALUES (1,2,'Desi','Admin','desi@cling-skincare.xyz','0812345678','Yogyakarta','user5-128x128.jpg','2017-07-23 09:44:33'),(2,3,'Mr. Little','CEO','reachme@sibudikecil.com','08115448548','Yogyakarta','Mr. Little-12383220_1652277971690582_2115043662_n.jpg','2017-07-23 11:35:09');
/*!40000 ALTER TABLE `karyawan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategori_perawatan`
--

DROP TABLE IF EXISTS `kategori_perawatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategori_perawatan` (
  `id_kategori_perawatan` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `deskripsi` text,
  PRIMARY KEY (`id_kategori_perawatan`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategori_perawatan`
--

LOCK TABLES `kategori_perawatan` WRITE;
/*!40000 ALTER TABLE `kategori_perawatan` DISABLE KEYS */;
INSERT INTO `kategori_perawatan` VALUES (1,'Hair Care','Perawatan Rambut'),(2,'Facials','Perawatan Wajah'),(3,'Skin Care','Perawatan Kulit'),(4,'Anti-Aging','Anti-Aging'),(5,'Bridal Make-Up','Rias Pengantin');
/*!40000 ALTER TABLE `kategori_perawatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kunjungan_pelanggan`
--

DROP TABLE IF EXISTS `kunjungan_pelanggan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kunjungan_pelanggan` (
  `id_kunjungan_pelanggan` int(11) NOT NULL AUTO_INCREMENT,
  `id_pelanggan` int(11) NOT NULL,
  `nomor_kunjungan` varchar(45) DEFAULT NULL,
  `waktu_kunjungan` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_kunjungan_pelanggan`),
  KEY `fk_kunjungan_to_pelanggan_idx` (`id_pelanggan`),
  CONSTRAINT `fk_kunjungan_to_pelanggan` FOREIGN KEY (`id_pelanggan`) REFERENCES `pelanggan` (`id_pelanggan`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kunjungan_pelanggan`
--

LOCK TABLES `kunjungan_pelanggan` WRITE;
/*!40000 ALTER TABLE `kunjungan_pelanggan` DISABLE KEYS */;
INSERT INTO `kunjungan_pelanggan` VALUES (1,1,'REG250717001','2017-07-25 14:41:16'),(2,2,'MBR2507170002','2017-07-25 14:41:25'),(3,1,'REG2507170003','2017-07-25 16:09:20'),(4,2,'MBR2507170004','2017-07-25 16:12:58'),(5,2,'MBR2707170005','2017-07-27 14:53:25'),(6,1,'REG2707170006','2017-07-27 14:54:05'),(7,4,'MBR2807170007','2017-07-27 17:58:20'),(8,5,'MBR2807170008','2017-07-27 17:58:35');
/*!40000 ALTER TABLE `kunjungan_pelanggan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pelanggan`
--

DROP TABLE IF EXISTS `pelanggan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pelanggan` (
  `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `nomor_pelanggan` varchar(50) DEFAULT NULL,
  `nama_pelanggan` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `nomor_telepon` varchar(20) DEFAULT NULL,
  `alamat` text,
  `tanggal_pendaftaran` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `foto` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_pelanggan`),
  KEY `fk_pelanggan_to_users_idx` (`id_user`),
  CONSTRAINT `fk_pelanggan_to_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pelanggan`
--

LOCK TABLES `pelanggan` WRITE;
/*!40000 ALTER TABLE `pelanggan` DISABLE KEYS */;
INSERT INTO `pelanggan` VALUES (1,1,'1234567890','Member Umum','member_umum@cling-skincare.xyz','1234567890','Yogyakarta','2017-07-25 08:50:27','profile-default.jpg'),(2,4,'2307170002','Member 1','member1@cling-skincare.xyz','08112345678','Yogyakarta','2017-07-23 12:49:50','-12383220_1652277971690582_2115043662_n.jpg'),(4,6,'2707170003','Tes Member 2','member2@cling-skincare.xyz','0987654321','Yogyakarta','2017-07-27 16:43:24',''),(5,5,'2707170005','Testing','tesmember@tes.tes','0987654321','Jakarta','2017-07-27 16:50:01','');
/*!40000 ALTER TABLE `pelanggan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pembayaran`
--

DROP TABLE IF EXISTS `pembayaran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pembayaran` (
  `id_pembayaran` int(11) NOT NULL AUTO_INCREMENT,
  `id_kunjungan` int(11) NOT NULL,
  `id_voucher` int(11) DEFAULT NULL,
  `total_harga` decimal(10,0) DEFAULT NULL,
  `total_pembayaran` decimal(10,0) DEFAULT NULL,
  `status_pembayaran` int(11) DEFAULT '2',
  `waktu_pembayaran` datetime DEFAULT NULL,
  `waktu_checkout` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_pembayaran`),
  KEY `fk_pembayaran_to_kunjungan_idx` (`id_kunjungan`),
  CONSTRAINT `fk_pembayaran_to_kunjungan` FOREIGN KEY (`id_kunjungan`) REFERENCES `kunjungan_pelanggan` (`id_kunjungan_pelanggan`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pembayaran`
--

LOCK TABLES `pembayaran` WRITE;
/*!40000 ALTER TABLE `pembayaran` DISABLE KEYS */;
INSERT INTO `pembayaran` VALUES (1,1,NULL,500000,500000,3,'2017-07-25 21:58:52','2017-07-25 14:53:21'),(2,2,NULL,500000,500000,3,'2017-07-25 23:59:07','2017-07-25 15:59:17'),(3,4,NULL,500000,500000,3,'2017-07-27 21:55:24','2017-07-27 14:53:49'),(4,7,NULL,500000,NULL,2,NULL,'2017-07-27 17:58:50');
/*!40000 ALTER TABLE `pembayaran` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perawatan`
--

DROP TABLE IF EXISTS `perawatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perawatan` (
  `id_perawatan` int(11) NOT NULL AUTO_INCREMENT,
  `id_kategori_perawatan` int(11) NOT NULL,
  `nama_perawatan` varchar(200) DEFAULT NULL,
  `fasilitas` text,
  `deskripsi` text,
  `harga` decimal(15,2) DEFAULT NULL,
  `foto` varchar(500) DEFAULT NULL,
  `poin` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_perawatan`),
  KEY `fk_perawatan_to_kategori_idx` (`id_kategori_perawatan`),
  CONSTRAINT `fk_perawatan_to_kategori` FOREIGN KEY (`id_kategori_perawatan`) REFERENCES `kategori_perawatan` (`id_kategori_perawatan`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perawatan`
--

LOCK TABLES `perawatan` WRITE;
/*!40000 ALTER TABLE `perawatan` DISABLE KEYS */;
INSERT INTO `perawatan` VALUES (1,4,'Cling Botanical Mesotherapy','Perawatan  menggunakan alat mesogun','Perawatan  menggunakan alat mesogun untuk memasukan serum botanical ke dalam kulit yang berfungsi untuk peremajaan, melembabkan,mencerahkan dan mengurangi noda hitam kulit.',500000.00,'treatment-img_Dx8I5LR.jpg',50),(2,4,'Cling Laser Dual Yellow','Laser CuBr kombinasi panjang gelombang 578 nanometer dan 511 nanometer','Laser CuBr kombinasi panjang gelombang 578 nanometer dan 511 nanometer. Panjang gelombang 578 adalah gelombang warna kuning,  bekerja di jaringan vaskuler, paling kuat diabsorpsi dalam hemoglobin. Panjang gelombang ini terbukti mengurangi atau menghilangkan kelainan vaskuler pada kulit. Sedangkan gelombang 511 nanometer berwarna hijau, diabsorpsi di sel pigmen melanin. Kelainan kulit yang bisa diatasi dengan Laser Dual Yellow: pigmentasi, bintik-bintik, tanda lahir berwarna coklat, angioma, rosacea, teleangiektasi dan tanda lahir merah, lentigines (age spot), keratosis seboroik.',500000.00,'treatment-img_Dx8I5LR.jpg',50),(3,2,'Cling Facial Oxygen Botanical',' oksigen dan serum botanical','Perawatan kulit wajah dengan menggunakan oksigen dan serum botanical yang berfungsi untuk memperbaiki metabolisme kulit, relaksasi, melancarkan aliran darah, dan mencerahkan kulit wajah.',300000.00,'treatment-img_MyCmUBL.jpg',30);
/*!40000 ALTER TABLE `perawatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pesan`
--

DROP TABLE IF EXISTS `pesan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pesan` (
  `id_pesan` int(11) NOT NULL AUTO_INCREMENT,
  `id_pelanggan` int(11) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `subyek` varchar(200) DEFAULT NULL,
  `isi_pesan` text,
  `waktu_kirim` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `id_penanggap` int(11) DEFAULT NULL,
  `isi_tanggapan` text,
  `waktu_tanggapan` datetime DEFAULT NULL,
  PRIMARY KEY (`id_pesan`),
  KEY `fk_pesan_to_pelanggan_idx` (`id_pelanggan`),
  CONSTRAINT `fk_pesan_to_pelanggan` FOREIGN KEY (`id_pelanggan`) REFERENCES `pelanggan` (`id_pelanggan`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pesan`
--

LOCK TABLES `pesan` WRITE;
/*!40000 ALTER TABLE `pesan` DISABLE KEYS */;
INSERT INTO `pesan` VALUES (2,2,'as@as.as','tes','<p>Tes</p>','2017-07-26 14:01:01',1,'asas','2017-07-26 21:01:01'),(3,2,'as@as.as','Tes From Member Area','<p><b>Tesss </b>tesss</p>','2017-07-26 14:04:17',2,'<p>aaaa</p>','2017-07-27 20:38:03'),(4,2,'as@as.as','Tes 2','Tes 2','2017-07-27 15:08:55',NULL,NULL,NULL),(5,5,'tesmember@tes.tes','Tes From Member Area','<p>TES</p>','2017-07-27 16:54:01',1,'<p>Ada apa?</p>','2017-07-27 23:55:08'),(6,1,'tes1@tes.tes','Tes dari Web','ini testing dari web','2017-07-27 18:22:02',NULL,NULL,NULL);
/*!40000 ALTER TABLE `pesan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `poin_pelanggan`
--

DROP TABLE IF EXISTS `poin_pelanggan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poin_pelanggan` (
  `id_poin_pelanggan` int(11) NOT NULL AUTO_INCREMENT,
  `id_pelanggan` int(11) NOT NULL,
  `jumlah_poin` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_poin_pelanggan`),
  KEY `fk_poin_to_pelanggan_idx` (`id_pelanggan`),
  CONSTRAINT `fk_poin_to_pelanggan` FOREIGN KEY (`id_pelanggan`) REFERENCES `pelanggan` (`id_pelanggan`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poin_pelanggan`
--

LOCK TABLES `poin_pelanggan` WRITE;
/*!40000 ALTER TABLE `poin_pelanggan` DISABLE KEYS */;
INSERT INTO `poin_pelanggan` VALUES (1,2,100);
/*!40000 ALTER TABLE `poin_pelanggan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promo`
--

DROP TABLE IF EXISTS `promo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promo` (
  `id_promo` int(11) NOT NULL AUTO_INCREMENT,
  `id_perawatan` int(11) NOT NULL,
  `nama_promo` varchar(200) DEFAULT NULL,
  `deskripsi` text,
  `penggunaan_poin` bigint(20) DEFAULT NULL,
  `potongan` decimal(10,0) DEFAULT NULL,
  `tanggal_mulai` datetime DEFAULT NULL,
  `tanggal_selesai` datetime DEFAULT NULL,
  `foto` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id_promo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promo`
--

LOCK TABLES `promo` WRITE;
/*!40000 ALTER TABLE `promo` DISABLE KEYS */;
/*!40000 ALTER TABLE `promo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `id_role` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `deskripsi` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_role`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (1,'admin','admin','2017-07-25 08:39:36'),(2,'member','member','2017-07-25 08:39:36');
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi_kunjungan`
--

DROP TABLE IF EXISTS `transaksi_kunjungan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaksi_kunjungan` (
  `id_transaksi_kunjungan` int(11) NOT NULL AUTO_INCREMENT,
  `id_kunjungan` int(11) NOT NULL,
  `id_perawatan` int(11) NOT NULL,
  `poin` int(11) DEFAULT NULL,
  `biaya` decimal(10,0) DEFAULT NULL,
  `status_transaksi` int(11) DEFAULT '1',
  `waktu_transaksi` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_transaksi_kunjungan`),
  KEY `fk_transaksi_to_kunjungan_idx` (`id_kunjungan`),
  CONSTRAINT `fk_transaksi_to_kunjungan` FOREIGN KEY (`id_kunjungan`) REFERENCES `kunjungan_pelanggan` (`id_kunjungan_pelanggan`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi_kunjungan`
--

LOCK TABLES `transaksi_kunjungan` WRITE;
/*!40000 ALTER TABLE `transaksi_kunjungan` DISABLE KEYS */;
INSERT INTO `transaksi_kunjungan` VALUES (1,1,1,50,500000,3,'2017-07-25 14:52:05'),(3,2,1,50,500000,3,'2017-07-25 15:59:14'),(4,3,1,50,500000,3,'2017-07-25 16:11:40'),(5,4,1,50,500000,3,'2017-07-27 14:53:40'),(6,6,2,50,500000,3,'2017-07-27 14:54:18'),(7,5,1,50,500000,1,'2017-07-27 17:54:39'),(8,7,3,50,500000,2,'2017-07-27 17:58:46');
/*!40000 ALTER TABLE `transaksi_kunjungan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_role` int(11) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_user_to_role_idx` (`id_role`),
  CONSTRAINT `fk_user_to_role` FOREIGN KEY (`id_role`) REFERENCES `role_user` (`id_role`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,2,'member_umum','123456','2017-07-25 08:40:21'),(2,1,'admin','$2y$10$7ML0QWaVAFh5xmz3mValLuy.K3gp.PfDtY0/LTJ8rhS2Em2XcNAHu','2017-07-23 09:22:38'),(3,1,'sibudikecil','$2y$10$cEVgRn5gGXjOtUh21WWgsu5sFoZkVkrzsFs0z3l.uyGpdTnbQUqmi','2017-07-23 10:54:04'),(4,2,'tesmember','$2y$10$6tPo371rGFI5CtjlP6nwk.fHTpJeHUtvQ3QhfzpAZqDH1jTBUoz8e','2017-07-23 12:17:48'),(5,2,'testing','$2y$10$y9w1qV4IpEppOo2aFwypaeP/TtMvN.Af8EYhYOYL4K1R14fIVgN4e','2017-07-27 16:19:35'),(6,2,'tesmember2','$2y$10$6N3ebDRQl2wCBaPJgniLjuTBCNhNGhS.BuLrcc8pYw50EcAWkBugW','2017-07-27 16:43:24'),(7,2,'testes','$2y$10$.sT.ygTOHP1oxbD0cdhhFO9M5DHJ1wc1xBMYTU0Fv6UPWZNfBn7aq','2017-07-27 18:16:56');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `voucher`
--

DROP TABLE IF EXISTS `voucher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voucher` (
  `id_voucher` int(11) NOT NULL AUTO_INCREMENT,
  `id_promo` int(11) NOT NULL,
  `kode_voucher` varchar(45) DEFAULT NULL,
  `status_valid` char(1) DEFAULT 'Y',
  `tanggal_dibuat` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `tanggal_dipakai` datetime DEFAULT NULL,
  PRIMARY KEY (`id_voucher`),
  KEY `fk_voucher_to_promo_idx` (`id_promo`),
  CONSTRAINT `fk_voucher_to_promo` FOREIGN KEY (`id_promo`) REFERENCES `promo` (`id_promo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `voucher`
--

LOCK TABLES `voucher` WRITE;
/*!40000 ALTER TABLE `voucher` DISABLE KEYS */;
/*!40000 ALTER TABLE `voucher` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-28  7:38:23
