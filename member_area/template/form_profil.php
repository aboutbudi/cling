<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Member
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Member</li><li class="active"><?php echo $pelanggan['id_pelanggan'] ? 'Ubah' : 'Tambah'; ?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header">
                        <a href="<?php echo URL_USER."member_area/pages/profil/"?>" class="pull-right btn btn-sm btn-danger" tooltip="view" alt="view"><i class="fa fa-backward"></i> Kembali</a>
                        <h3 class="box-title"><?php echo $pelanggan['id_pelanggan'] ? 'Ubah Member' : 'Tambah Member'; ?></h3>
                    </div>
                    <div class="box-body">
                        <form enctype="multipart/form-data" action="<?php echo $pelanggan['id_pelanggan'] ? URL_USER."member_area/pages/profil/ubah.php" : "#"; ?>" method="post" class="form-horizontal">
                            <?php echo $pelanggan['id_pelanggan'] ? '<div class="form-group">
                                <label for="email" class="col-sm-2 control-label">Nomor Pelanggan</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="email" name="email" value="'.$pelanggan['nomor_pelanggan'].'" disabled>
                                </div>
                            </div>' : '' ?>
                            <div class="form-group">
                                <label for="nama_pelanggan" class="col-sm-2 control-label">Nama Pelanggan</label>
                                <div class="col-sm-10">
                                    <input type="hidden" class="form-control" id="id_pelanggan" name="id_pelanggan" value="<?php echo !empty($pelanggan) ? $pelanggan['id_pelanggan'] : ''; ?>">
                                    <input type="text" class="form-control" id="nama_pelanggan" name="nama_pelanggan" value="<?php echo !empty($pelanggan) ? $pelanggan['nama_pelanggan'] : ''; ?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-sm-2 control-label">email</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="email" name="email" value="<?php echo !empty($pelanggan) ? $pelanggan['email'] : ''; ?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nomor_telepon" class="col-sm-2 control-label">Nomor Telepon</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" id="nomor_telepon" name="nomor_telepon" value="<?php echo !empty($pelanggan) ? $pelanggan['nomor_telepon'] : ''; ?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="alamat" class="col-sm-2 control-label">Alamat</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="alamat" name="alamat" value="<?php echo !empty($pelanggan) ? $pelanggan['alamat'] : ''; ?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="alamat" class="col-sm-2 control-label">Foto</label>
                                <div class="col-sm-3">
                                    <?php echo $pelanggan['foto'] ? '<img class="img-thumbnail" src="'.URL_USER."upload/".$pelanggan['foto'].'"/><br><br>' : '' ?>
                                    <input name="foto" type="file" id="foto"> 
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12" style="text-align:right;">
                                    <a href="<?php echo URL_USER."member_area/pages/profil/"?>" class="btn btn-danger">Cancel</a>
                                    <button type="reset" class="btn btn-warning">Reset</button>
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>