<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pesan
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-envelope-o"></i> Home</a></li>
        <li class="active">Pesan</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <a href="<?php echo URL_USER."member_area/pages/pesan/tambah.php"?>" class="btn btn-primary btn-block margin-bottom">Compose</a>

          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Folders</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="<?php echo URL_USER."member_area/pages/pesan/"?>"><i class="fa fa-inbox"></i> Inbox
                  <span class="label label-primary pull-right"><?php echo $jml_pesan['jumlah_pesan_masuk']>0 ? $jml_pesan['jumlah_pesan_masuk'] :'';?></span></a></li>
                <li><a href="<?php echo URL_USER."member_area/pages/pesan/terkirim.php"?>"><i class="fa fa-envelope-o"></i> Sent 
                <span class="label label-primary pull-right"><?php echo $jml_sent['jumlah_pesan_terkirim']>0 ? $jml_sent['jumlah_pesan_terkirim'] :'';?></span></a></li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title">Pesan Terkirim</h3>

                    <!-- <div class="box-tools pull-right">
                        <div class="has-feedback">
                        <input type="text" class="form-control input-sm" placeholder="Search Mail">
                        <span class="glyphicon glyphicon-search form-control-feedback"></span>
                        </div>
                    </div> -->
                <!-- /.box-tools -->
                </div>
                <div class="box-body no-padding">
                    <!-- <div class="mailbox-controls">
                        
                        <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
                        </button>
                        <div class="btn-group">
                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                        </div>
                        
                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                        <div class="pull-right">
                        1-50/200
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                            <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                        </div>
                        
                        </div>
                        
                    </div> -->
                    <div class="table-responsive mailbox-messages">
                        <table class="table table-hover table-striped">
                        <tbody>
                        <?php 
                        if (is_array($pesan_masuk) || is_object($pesan_masuk)){
                            foreach($pesan_masuk as $pesan_masuk)
                            {
                        ?>
                        <tr>
                            <td class="mailbox-star"><a href="#"><i class="fa fa-inbox text-yellow"></i></a></td>
                            <td class="mailbox-name"><a href="<?php echo URL_USER."member_area/pages/pesan/detail.php?id_pesan=".$pesan_masuk['id_pesan']."&tipe=sent"?>"><?php echo "Dari : ".$pesan_masuk['nama_pelanggan']?></a></td>
                            <td class="mailbox-subject"><b><?php echo "Subyek : ".$pesan_masuk['subyek']?></b>
                            </td>
                            <td class="mailbox-date" style="max-width:120px;"><?php echo $pesan_masuk['waktu_balas']?></td>
                        </tr>
                        <?php 
                            }
                            }
                        ?>
                        </tbody>
                        </table>
                        <!-- /.table -->
                    </div>
                    <!-- /.mail-box-messages -->
                    </div>
                    <!-- /.box-body -->
            </div>
        </div>
    </section>
</div>