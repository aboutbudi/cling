<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-4">
          <div class="box box-danger direct-chat direct-chat-danger">
            <div class="box-header with-border">
            <i class="fa fa-inbox"></i> 
              <h3 class="box-title">Pesan Masuk</h3>

              <div class="box-tools pull-right">
                <span data-toggle="tooltip" title="<?php echo $jml_pesan['jumlah_pesan_masuk']?> New Messages" class="badge bg-red"><?php echo $jml_pesan['jumlah_pesan_masuk']?></span>
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <!-- Conversations are loaded here -->
              <div class="direct-chat-messages">
                <!-- Message to the right -->
                <?php 
                if (is_array($pesan_masuk) || is_object($pesan_masuk)){
                    foreach($pesan_masuk as $pesan_masuk)
                    {
                ?>
                <div class="direct-chat-msg right">
                  <div class="direct-chat-info clearfix">
                    <span class="direct-chat-name pull-right"><?php echo $pesan_masuk['nama_karyawan']?></span>
                    <span class="direct-chat-timestamp pull-left"><?php echo $pesan_masuk['waktu_balas']?></span>
                  </div>
                  <!-- /.direct-chat-info -->
                  <img class="direct-chat-img" src="<?php echo $pesan_masuk['foto'] ? URL_USER."img/".$pesan_masuk['foto'] : URL_USER."img/profile-default.jpg"?>" alt="Message User Image"><!-- /.direct-chat-img -->
                  <div class="direct-chat-text">
                    <?php echo $pesan_masuk['subyek']?>
                  </div>
                  <!-- /.direct-chat-text -->
                </div>
                <?php
                    }}
                ?>
                <!-- /.direct-chat-msg -->
              </div>
              <!--/.direct-chat-messages-->
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Whats New</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body chat" id="chat-box">
              <!-- post item -->
              <?php 
                  if (is_array($blog) || is_object($blog)){
                  foreach($blog as $blog)
                  {
              ?>
              <div class="item">
                <img src="<?php echo $blog['foto'] ? URL_USER."upload/".$blog['foto'] : URL_USER."images/11.jpg"?>" alt="user image" class="online">
                <p class="message">
                  <a href="<?php echo URL_ADMIN."controller/blog/detail.php?id_blog=".$blog['id_blog']; ?>" class="name">
                    <small class="text-muted pull-right"><i class="fa fa-calendar"></i> <?php echo $blog['tanggal_posting'] ?></small>
                    <?php echo $blog['judul'] ?>
                  </a>
                  Di posting oleh : <?php echo $blog['penulis'] ?>
                </p>
              </div>
              <?php 
                }
                }
              ?>
            </div>
            <div class="box-footer text-center">
              <a href="<?php echo URL_ADMIN."controller/blog" ?>" class="uppercase">Lihat Semua Post</a>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <div class="col-md-4">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-black" style="background: url('<?php echo URL_USER?>img/photo1.png') center center;">
              <h3 class="widget-user-username"><?php echo $user['nama_pelanggan']?></h3>
              <h5 class="widget-user-desc"><?php echo $user['username']?></h5>
            </div>
            <div class="widget-user-image">
              <img class="img-circle" src="<?php echo $user['foto'] ? URL_USER."upload/".$user['foto'] : URL_USER."img/profile-default.jpg"?>" alt="User Avatar">
            </div>
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-12">
                  <div class="description-block">
                    <h3 class="widget-user-username"><?php echo $user['nama_pelanggan']?></h3>
                    <h5 class="description-header"><?php echo $user['nomor_pelanggan'].' - '.$user['email']?></h5>
                    <span class="description-text"><?php echo "Member sejak , ".$user['tanggal_gabung']?></span>
                  </div>
                  <!-- /.description-block -->
                </div>
              </div>
              <!-- /.row -->
            </div>
          </div>

          <div class="small-box bg-red">
            <div class="inner" style="padding-left:20px ;">
              <h3>MY POIN</h3>
              <h4><?php echo $user['jumlah_poin'] ?  $user['jumlah_poin']." POIN" : "BELUM ADA POIN"?></h4>
            </div>
            <div class="icon" style="padding:20px 0;">
              <i class="fa fa-gift"></i>
            </div>
            <a href="#" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
          <!-- /.widget-user -->
        </div>
      </div>
    </section>
</div>
