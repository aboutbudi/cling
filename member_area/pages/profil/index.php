<?php
    define('PAGE_TITLE', 'Profil');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');

    include_once('../../../config/controller.php');

    // Initialize the session
    session_start();
    
    // If session variable is not set it will redirect to login page
    if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 1){
        header("location:".URL_USER);
        exit;
    }

    $username = $_SESSION['username'];
    $query = "SELECT users.username,pelanggan.*,DATE_FORMAT(pelanggan.tanggal_pendaftaran, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN pelanggan ON users.id = pelanggan.id_user WHERE users.username= '".$username."'";
    $user =  selectDetail($query);
    
    $query = "select *, DATE_FORMAT(tanggal_pendaftaran, \"%e %M %Y\") as tanggal_mendaftar, poin_pelanggan.jumlah_poin FROM pelanggan LEFT JOIN poin_pelanggan ON pelanggan.id_pelanggan = poin_pelanggan.id_pelanggan WHERE pelanggan.id_pelanggan=" .$user['id_pelanggan'];
    $pelanggan_detail = selectDetail($query); 
    
    $content_page='../../template/index_profil.php';

    include_once('../../layout/main_layout.php');
?>