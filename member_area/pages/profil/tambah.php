<?php
    define('PAGE_TITLE', 'Member > Tambah');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');
    define('PATH_FILE', $_SERVER["DOCUMENT_ROOT"].'/cling/upload/');

    // Initialize the session
    session_start();
    
        // If session variable is not set it will redirect to login page
    if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
            header("location:".URL_ADMIN."controller/auth/login.php");
            exit;
    }
    if($_SESSION['id_role'] == 1){
        header("location:".URL_USER);
        exit;
    }
    
    include_once('../../../config/controller.php');
    
    $username = $_SESSION['username'];
    $query = "SELECT * FROM users WHERE username= '".$username."'";
    $user =  selectDetail($query);

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $id_user=$user['id'];
        $nama_pelanggan = isset($_POST['nama_pelanggan']) ? $_POST['nama_pelanggan'] : "";
        $email = isset($_POST['email']) ? $_POST['email'] : "";
        $nomor_telepon = isset($_POST['nomor_telepon']) ? $_POST['nomor_telepon'] : "";
        $alamat = isset($_POST['alamat']) ? $_POST['alamat'] : "";

        $query2 = "SELECT * FROM pelanggan order by id_pelanggan desc limit 1";
        $tes = selectDetail($query2);

        $last_record=$tes["id_pelanggan"];
        $no_reg="";
        if($last_record<10){
            $no_reg= date("dmy")."000".$last_record+1;
        }elseif($last_record>=10&&$last_record<100){
            $no_reg= date("dmy")."00".$last_record+1;
        }else{
            $no_reg= date("dmy")."0".$last_record+1;
        }
        
        $nama_file = $_FILES['foto']['name'];
        $ukuran_file = $_FILES['foto']['size'];
        $tipe_file = $_FILES['foto']['type'];
        $tmp_file = $_FILES['foto']['tmp_name'];

        $nomor_pelanggan = $no_reg;

        $uploaddir = '../../../upload/';
        
        if($nama_file==""){
            $values = array($id_user,$nomor_pelanggan,$nama_pelanggan,$email,$nomor_telepon,$alamat,$foto);
            $columns = array('id_user','nomor_pelanggan', 'nama_pelanggan', 'email', 'nomor_telepon', 'alamat', 'foto');
            
            insert('pelanggan', $values, $columns);

            echo "<meta http-equiv='refresh' content='0;url=".URL_USER."member_area/pages/profil/'>";
        }else{
            $foto = $nomor_pelanggan.'-'.$nama_file;
            $uploadfile = $uploaddir . $foto;

            
            if($tipe_file == "image/jpg" ||$tipe_file == "image/jpeg" || $tipe_file == "image/png"){
                if($ukuran_file <= 1000000){
                    if(move_uploaded_file($tmp_file, $uploadfile)){
                        $values = array($id_user,$nomor_pelanggan,$nama_pelanggan,$email,$nomor_telepon,$alamat,$foto);
                        $columns = array('id_user','nomor_pelanggan', 'nama_pelanggan', 'email', 'nomor_telepon', 'alamat', 'foto');
                        
                        insert('pelanggan', $values, $columns);

                        echo "<meta http-equiv='refresh' content='0;url=".URL_USER."member_area/pages/profil/'>";
                    }else{
                        echo "<script>alert(\"File gagal diupload\");</script>";
                    }
                }else{
                    echo "<script>alert(\"Ukuran file terlalu besar\");</script>";
                }
            }else{
                echo "<script>alert(\"File yang anda masukan bukan gambar\");</script>";
            }
        }
    }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Cling SkinCare | Update Profil</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo URL_USER ?>css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo URL_USER ?>plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#">Cling SkinCare</a>
  </div>
  <!-- /.login-logo -->
  <?php
  include $page_content;
  ?>
    <div class="login-box-body">
        <h4 class="login-box-msg">Lengkapi data anda di sini</h4>
        <form enctype="multipart/form-data" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group has-feedback">
                <input type="text" name="nama_pelanggan" class="form-control" placeholder="nama">
                <span class="fa fa-user-o form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="email" name="email" class="form-control" placeholder="email">
                <span class="fa fa-envelope-o form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="text" name="nomor_telepon" class="form-control" placeholder="nomor_telepon">
                <span class="fa fa-phone form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="text" name="alamat" class="form-control" placeholder="alamat">
                <span class="fa fa-home form-control-feedback"></span>
            </div>
            <div class="form-group">
                <input name="foto" type="file" id="foto"> 
            </div>
            
            <div class="row">
                <div class="col-xs-8">
                
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
                </div>
                <!-- /.col -->
            </div>
        </form>
    </div>
  
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?php echo URL_USER ?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo URL_USER ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo URL_USER ?>plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>