<?php
    define('PAGE_TITLE', 'Profil');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');

    include_once('../../../config/controller.php');

    // Initialize the session
    session_start();
    
    // If session variable is not set it will redirect to login page
    if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 1){
        header("location:".URL_USER);
        exit;
    }
    $username = $_SESSION['username'];
    $query = "SELECT users.username,pelanggan.*,DATE_FORMAT(pelanggan.tanggal_pendaftaran, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN pelanggan ON users.id = pelanggan.id_user WHERE users.username= '".$username."'";
    $user =  selectDetail($query);
    
    $content_page='../../template/form_profil.php';

    if($_SERVER['REQUEST_METHOD'] == 'GET'){
        //get anggota by id 
        $id_pelanggan = isset($_GET['id_pelanggan']) ? $_GET['id_pelanggan'] : "";
        $condition = 'id_pelanggan='.$id_pelanggan;
        $results = select('pelanggan', null, $condition);
        $pelanggan = count($results) > 0 ? $results[0] : array();
    }

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $id_pelanggan = isset($_POST['id_pelanggan']) ? $_POST['id_pelanggan'] : "";
        $nama_pelanggan = isset($_POST['nama_pelanggan']) ? $_POST['nama_pelanggan'] : "";
        $email = isset($_POST['email']) ? $_POST['email'] : "";
        $nomor_telepon = isset($_POST['nomor_telepon']) ? $_POST['nomor_telepon'] : "";
        $alamat = isset($_POST['alamat']) ? $_POST['alamat'] : "";
        
        $nama_file = $_FILES['foto']['name'];
        $ukuran_file = $_FILES['foto']['size'];
        $tipe_file = $_FILES['foto']['type'];
        $tmp_file = $_FILES['foto']['tmp_name'];
        
        $uploaddir = '../../../upload/';
        if($nama_file==""){
            $columns = array(
                'nama_pelanggan'=>$nama_pelanggan,
                'email'=>$email,
                'nomor_telepon'=>$nomor_telepon,
                'alamat'=>$alamat
            );
            
            $condition = "id_pelanggan = ".$id_pelanggan;
            update('pelanggan',$columns, $condition);
            echo "<meta http-equiv='refresh' content='0;url=".URL_USER."member_area/pages/profil/'>";
        }else{
            $foto = $nomor_pelanggan.'-'.$nama_file;
            $uploadfile = $uploaddir . $foto;

            
            if($tipe_file == "image/jpg" ||$tipe_file == "image/jpeg" || $tipe_file == "image/png"){
                if($ukuran_file <= 1000000){
                    if(move_uploaded_file($tmp_file, $uploadfile)){
                        $columns = array(
                            'nama_pelanggan'=>$nama_pelanggan,
                            'email'=>$email,
                            'nomor_telepon'=>$nomor_telepon,
                            'alamat'=>$alamat, 
                            'foto'=>$foto
                        );
                        
                        $condition = "id_pelanggan = ".$id_pelanggan;
                        update('pelanggan',$columns, $condition);
                        echo "<meta http-equiv='refresh' content='0;url=".URL_USER."member_area/pages/profil/'>";
                    }else{
                        echo "<script>alert(\"File gagal diupload\");</script>";
                    }
                }else{
                    echo "<script>alert(\"Ukuran file terlalu besar\");</script>";
                }
            }else{
                echo "<script>alert(\"File yang anda masukan bukan gambar\");</script>";
            }
        }
    }

    include_once('../../layout/main_layout.php');
?>