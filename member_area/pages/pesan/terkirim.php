<?php
    define('PAGE_TITLE', 'Pesan');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');

    include_once('../../../config/controller.php');

    // Initialize the session
    session_start();
    
    // If session variable is not set it will redirect to login page
    if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 1){
        header("location:".URL_USER);
        exit;
    }
    $username = $_SESSION['username'];
    $query = "SELECT users.username,pelanggan.*,DATE_FORMAT(pelanggan.tanggal_pendaftaran, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN pelanggan ON users.id = pelanggan.id_user WHERE users.username= '".$username."'";
    $user =  selectDetail($query);

    $query_pesan_masuk = "SELECT a.*,b.nama_pelanggan,DATE_FORMAT(a.waktu_kirim, \"%e %M %Y %r\") AS waktu_balas FROM pesan a
                            INNER JOIN pelanggan b ON a.id_pelanggan = b.id_pelanggan 
                            WHERE a.id_pelanggan=".$user['id_pelanggan']." ORDER BY a.waktu_kirim DESC";
    $pesan_masuk =  selectBySql($query_pesan_masuk);
    
    $query_jml_psn = "SELECT COUNT(id_pesan) AS jumlah_pesan_masuk FROM pesan WHERE id_penanggap IS NOT NULL AND id_pelanggan=".$user['id_pelanggan'];
    $jml_pesan =  selectDetail($query_jml_psn);
    
    $query_jml_sent = "SELECT COUNT(id_pesan) AS jumlah_pesan_terkirim FROM pesan WHERE id_pelanggan=".$user['id_pelanggan'];
    $jml_sent =  selectDetail($query_jml_sent);
    
    $content_page='../../template/list_pesan_terkirim.php';

    include_once('../../layout/main_layout.php');
?>