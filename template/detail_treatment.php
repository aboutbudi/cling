<!-- about -->
	<!--start-about-->
	<div class="about">
		<div class="container">
			<div class="about-top heading">
				<h3 class="tittle-one"><?php echo $perawatan['nama_perawatan'] ?></h3>
			</div>
			<div class="about-bottom l-grids text-center">
				<figure class="effect-bubba">
					<img src="<?php echo $perawatan['foto'] ? URL_USER."upload/".$perawatan['foto'] : URL_USER."images/ban2.jpg" ?>" alt=""/>
					<figcaption>
						<h4>Cling</h4>
						<p>Kecentikan tidak harus dari wajah tetapi juga dari hati.</p>																
					</figcaption>			
				</figure>
			</div>
			<h5><?php echo $perawatan['fasilitas'] ?></h5>
            <p><?php echo $perawatan['deskripsi'] ?></p>
            <p><?php echo 'Harga : Rp. '.$perawatan['harga'] ?></p>
            <p><?php echo 'Poin : '.$perawatan['poin'] ?></p>
		</div>
	</div>	
<!--//about-->