<!-- about -->
	<!--start-about-->
	<div class="about">
		<div class="container">
			<div class="about-top heading">
				<h3 class="tittle-one"><?php echo $blog['judul'] ?></h3>
			</div>
			<div class="about-bottom l-grids text-center">
				<figure class="effect-bubba">
					<img src="<?php echo $blog['foto'] ? URL_USER."upload/".$blog['foto'] : URL_USER."images/ban2.jpg" ?>" alt="<?php echo $blog['judul'] ?>"/>
					<figcaption>
						<h4>Cling</h4>
						<p>Kecentikan tidak harus dari wajah tetapi juga dari hati.</p>																
					</figcaption>			
				</figure>
			</div>
            <?php echo $blog['konten'] ?>
            <p>Author : <?php echo $blog['penulis'] ?> | Tanggal Post : <?php echo $blog['tanggal_posting'] ?></p>
		</div>
	</div>	
<!--//about-->