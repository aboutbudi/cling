<!-- about -->
	<!--start-about-->
	<div class="about">
		<div class="container">
			<div class="about-top heading">
				<h3 class="tittle-one">Tentang Kami</h3>
			</div>
			<div class="about-bottom l-grids text-center">
				<figure class="effect-bubba">
					<img src="<?php echo URL_USER?>images/ban2.jpg" alt=""/>
					<figcaption>
						<h4>Cling</h4>
						<p>Kecentikan tidak harus dari wajah tetapi juga dari hati.</p>																
					</figcaption>			
				</figure>
			</div>
			<h5>Sed hendrerit dui eget lacus imperdiet lacinia. Sed eleifend,
				libero ac pellentesque ornare, velit velit tempor nisi, quis cursus dolor velit condimentum elit.</h5>
			<p>Duis semper sodales dictum. Praesent facilisis dui 
				a viverra fringilla. Cras nisi felis, sollicitudin
				non bibendum iaculis, tincidunt id erat. Morbi tincidunt 
				erat tellus, ut rhoncus sem malesuada et. Duis vitae 
				auctor arcu. Aenean in tellus ac quam maximus dignissim. 
				Vivamus dapibus eget mi ut consectetur. Donec risus nunc, 
				pulvinar eu viverra vitae, placerat at nisi. Duis a mollis enim. 
				Pellentesque sollicitudin dignissim ex, sit amet ornare dui fringilla eu.</p>
		</div>
	</div>	
<!--//about-->