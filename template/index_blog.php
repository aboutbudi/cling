<div class="tips">
	<div class="container">
		<h3 class="tittle-one">Blog</h3>
		<div class="tip-grids">
            <?php 
                if (is_array($blog) || is_object($blog)){
                    foreach($blog as $blog)
                    {
            ?>
            <div class="col-md-4 tip-grid" style="margin:20px 0 20px 0;">
				<figure class="effect-julia">
                    <a href="<?php echo URL_USER."pages/blog/detail.php?id_blog=".$blog['id_blog'];?>">
					<img src="<?php echo $blog['foto'] ? URL_USER."upload/".$blog['foto'] : URL_USER."images/9.jpg" ?>" alt="" />
						<figcaption>
							<h4 style="font-size:25px;"><?php echo $blog['judul'] ?></h4>
								<p>Author :<?php echo $blog['penulis'] ?></p>
                        </figcaption>
                    </a>			
				</figure>
            </div>
            <?php
                    }
                }
            ?>
			
			<div class="clearfix"></div>
		</div>
	</div>
</div>
