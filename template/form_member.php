<div class="about">
	<div class="container">
        <h3 class="tittle-one">Pendaftaran Member</h3>
    </div>
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <h5>Silahkan masukkan data diri dengan benar !</h5>
            <form enctype="multipart/form-data" action="<?php echo $pelanggan['id_pelanggan'] ? URL_USER."pages/member/ubah.php" : URL_USER."pages/member/tambah_member.php"; ?>" method="post" class="form-horizontal">
                <?php echo $pelanggan['id_pelanggan'] ? '<div class="form-group">
                    <label for="email" class="col-sm-3 control-label">Nomor Pelanggan</label>
                    <div class="col-sm-9">
                        <input type="email" class="form-control" id="email" name="email" value="'.$pelanggan['nomor_pelanggan'].'" disabled>
                    </div>
                </div>' : '' ?>
                <div class="form-group">
                    <label for="nama_pelanggan" class="col-sm-3 control-label">Nama Pelanggan</label>
                    <div class="col-sm-9">
                        <input type="hidden" class="form-control" id="id_pelanggan" name="id_pelanggan" value="<?php echo !empty($pelanggan) ? $pelanggan['id_pelanggan'] : ''; ?>">
                        <input type="text" class="form-control" id="nama_pelanggan" name="nama_pelanggan" value="<?php echo !empty($pelanggan) ? $pelanggan['nama_pelanggan'] : ''; ?>" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">email</label>
                    <div class="col-sm-9">
                        <input type="email" class="form-control" id="email" name="email" value="<?php echo !empty($pelanggan) ? $pelanggan['email'] : ''; ?>" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="nomor_telepon" class="col-sm-3 control-label">Nomor Telepon</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control" id="nomor_telepon" name="nomor_telepon" value="<?php echo !empty($pelanggan) ? $pelanggan['nomor_telepon'] : ''; ?>" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="alamat" class="col-sm-3 control-label">Alamat</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="alamat" name="alamat" value="<?php echo !empty($pelanggan) ? $pelanggan['alamat'] : ''; ?>" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="alamat" class="col-sm-3 control-label">Foto</label>
                    <div class="col-sm-9">
                        <input name="foto" type="file" id="foto" value="<?php echo !empty($pelanggan) ? $pelanggan['foto'] : ''; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12" style="text-align:right;">
                        <a href="<?php echo URL_USER."pages/member/"?>" class="btn btn-danger">Cancel</a>
                        <button type="reset" class="btn btn-warning">Reset</button>
                        <button type="submit" class="btn btn-success">Daftar</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-2">
        </div>
    </div>
</div>