<div class="banner-bottom">
	<div class="container">
		<h3 class="tittle-one">Selamat Datang Cling Skin Care</h3>
		<p>Klinik kecantikan cling menyediakan berbagai macam jenis perawatan atau treatment 
		yang mungkin cocok buat kulit anda. silahkan mencoba untuk datang atau melihat-lihat jenis treatment di tempat kami.
		</p>
		<div class="bottom-grids">
			<div class="col-md-3 bottom-grid">
				<img src="images/11.jpg" alt="" />
			</div>
			<div class="col-md-3 bottom-grid grid-one">
				<h4>Cling Jet Peel</h4>
				<p>Perawatan kulit menggunakan serum botanical anti-aging dan tekanan tinggi oksigen
				dengan kecepatan aliran 200m/detik. kulit menjadi lebih sehat, segar berseri, kenyal dan bersinar</p>
			</div>
			<div class="col-md-3 bottom-grid">
				<img src="images/14.jpg" alt="" />
			</div>
			<div class="col-md-3 bottom-grid grid-one">
				<h4>Cling Botanical Mesotherapy</h4>
				<p>Perawatan menggunakan alat mesogun untuk memasukkan serum botanical ke dalam kulit yang berfungsi untuk peremajaan,melembabkan, 
				mencerahkan, dan mengurangi noda hitam kulit.</p>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- features -->
<div class="feature">
	<div class="container">
		<h3 class="tittle-one">Features</h3>
		<div class="feature-grids">
			<div class="col-md-3 feature-grid text-center">
				<div class="feature-grid-one">
					<a href="#" class="hi-icon hi-icon-archive glyphicon glyphicon-pencil"> </a>
					<h4>Cling Passive Exercise(PE)</h4>
					<p> Perawatan untuk prngrncangan otot-otot di area wajah dan leher menggunakan 
					electrostimulation dan galvanic current.</p>
				</div>
			</div>
			<div class="col-md-3 feature-grid text-center">
				<div class="feature-grid-two">
					<a href="#" class="hi-icon hi-icon-archive glyphicon glyphicon-user"> </a>
					<h4>Cling Oxygen Botanical</h4>
					<p> Perawatan wajah menggunakan oksigen dan serum botanical yang berfungsi untuk memperbaiki metabolisme kulit .</p>
				</div>
			</div>
			<div class="col-md-3 feature-grid text-center">
				<div class="feature-grid-one">
					<a href="#" class="hi-icon hi-icon-archive glyphicon glyphicon-star"> </a>
					<h4>Cling Radiofrequency</h4>
					<p> Perawatan menggunakan teknologi radiofrekuensi untuk pengencangan dan mengurangi shagging pada wajah. </p>
				</div>
			</div>
			<div class="col-md-3 feature-grid text-center">
				<div class="feature-grid-two">
					<a href="#" class="hi-icon hi-icon-archive glyphicon glyphicon-map-marker"> </a>
					<h4>Cling Wet Diamond Peel</h4>
					<p> Perawatan kulit yang menggunakan teknik debarmasi da memasukkan serum
					botanical kedalam lapisan kulit.</p>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- //features -->