<div class="tips">
	<div class="container">
		<h3 class="tittle-one">Perawatan</h3>
		<div class="tip-grids">
            <?php 
                if (is_array($perawatan) || is_object($perawatan)){
                    foreach($perawatan as $perawatan)
                    {
            ?>
            <div class="col-md-4 tip-grid" style="margin:20px 0 20px 0;">
				<figure class="effect-julia">
                    <a href="<?php echo URL_USER."pages/treatment/detail.php?id_perawatan=".$perawatan['id_perawatan'];?>">
					<img src="<?php echo $perawatan['foto'] ? URL_USER."upload/".$perawatan['foto'] : URL_USER."images/9.jpg" ?>" alt="" />
						<figcaption>
							<h4 style="font-size:25px;"><?php echo $perawatan['nama_perawatan'] ?></h4>
								<p><?php echo $perawatan['fasilitas'] ?></p>
								<p><?php echo $perawatan['harga'] ?></p>
                        </figcaption>
                    </a>			
				</figure>
            </div>
            <?php
                    }
                }
            ?>
			
			<div class="clearfix"></div>
		</div>
	</div>
</div>
