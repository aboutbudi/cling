<?php
    define('PAGE_TITLE', 'Treatment');
    define('PAGE_LOCATION', 'treatment');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');
    
    include_once('../../config/controller.php');

    $content_page='../../template/detail_treatment.php';

    $id_perawatan  = $_GET["id_perawatan"];
    $query = "SELECT perawatan.*, kategori_perawatan.nama AS kategori_perawatan FROM perawatan INNER JOIN kategori_perawatan ON perawatan.id_kategori_perawatan= kategori_perawatan.id_kategori_perawatan WHERE perawatan.id_perawatan=" .$id_perawatan;
    $perawatan = selectDetail($query);

    include_once('../../layout/main_layout.php');
?>