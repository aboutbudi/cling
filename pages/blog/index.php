<?php
    define('PAGE_TITLE', 'Blog');
    define('PAGE_LOCATION', 'blog');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');
    
    include_once('../../config/controller.php');

    $content_page='../../template/index_blog.php';

    $blog = array();
    $query = "SELECT *, DATE_FORMAT(tanggal_post, \"%e %M %Y\") as tanggal_posting FROM blog ORDER BY tanggal_post DESC LIMIT 9";
    $blog =  selectBySql($query);

    include_once('../../layout/main_layout.php');
?>