<?php
    define('PAGE_TITLE', 'Blog');
    define('PAGE_LOCATION', 'blog');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');
    
    include_once('../../config/controller.php');

    $content_page='../../template/detail_blog.php';

    $id_blog  = $_GET["id_blog"];
    $query = "SELECT *, DATE_FORMAT(tanggal_post, \"%e %M %Y\") as tanggal_posting FROM blog WHERE id_blog=".$id_blog;
    $blog = selectDetail($query);

    include_once('../../layout/main_layout.php');
?>