<?php
    define('PAGE_TITLE', 'Contact');
    define('PAGE_LOCATION', 'kontak');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');
    
    include_once('../../config/controller.php');

    $content_page='../../template/kontak.php';

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $id_pelanggan=1;
        // $pengirim = isset($_POST['pengirim']) ? $_POST['pengirim'] : "";
        $email = isset($_POST['email']) ? $_POST['email'] : "";
        $subyek = isset($_POST['subyek']) ? $_POST['subyek'] : "";
        $isi_pesan = isset($_POST['isi_pesan']) ? $_POST['isi_pesan'] : "";
                
        $values = array($id_pelanggan,$email,$subyek,$isi_pesan);
        $columns = array('id_pelanggan', 'email','subyek','isi_pesan');
        
        insert('pesan', $values, $columns);
        echo "<meta http-equiv='refresh' content='0;url=".URL_USER."pages/kontak/'>";
    } 

    include_once('../../layout/main_layout.php');
?>