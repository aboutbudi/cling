<?php
    define('PAGE_TITLE', 'Member');
    define('PAGE_LOCATION', 'member');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');
    
    include_once('../../config/controller.php');

    $content_page='../../template/index_member.php';

    include_once('../../layout/main_layout.php');
?>