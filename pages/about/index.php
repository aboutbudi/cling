<?php
    define('PAGE_TITLE', 'About');
    define('PAGE_LOCATION', 'about');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');
    
    include_once('../../config/controller.php');

    $content_page='../../template/index_about.php';

    include_once('../../layout/main_layout.php');
?>